package br.srv.rcs.onibus;

import java.text.NumberFormat;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.apmem.tools.layouts.FlowLayout;
import org.apmem.tools.layouts.FlowLayout.LayoutParams;
import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Linha;

@EFragment(R.layout.fragment_item_linha)
public class LinhaItemFragment extends Fragment {

	protected Linha linha;
	
//	@ViewById
//	protected ImageView image;

	@ViewById
	protected TextView text;

	@ViewById
	protected FlowLayout grid;

	protected boolean isValorVisible = true;
	protected boolean isProximoHorarioVisible = true;
	protected boolean isSaidaVisible = true;
	protected boolean isAlarmeVisible = true;

	public LinhaItemFragment() {
		// TODO Auto-generated constructor stub
	}

	@AfterViews
	protected void load() {
		if (linha != null)
			carregarUi();
	}

	@SuppressLint("InflateParams")
	@UiThread
	protected void carregarUi() {

		grid.removeAllViews();
		Horario proximoHorario = linha.getProximoHorario();
		LocalTime horarioSaida = proximoHorario.getHorarioSaida().toLocalTime();
		text.setText(linha.getNome());

		if (isValorVisible) {
			// Valor --Sempre virá
			FrameLayout valor = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_item_linha_info, null);
			valor.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			TextView valorText = (TextView) valor.findViewById(R.id.text);
			valorText.setBackgroundResource(R.color.mark_green);
			grid.addView(valor);
			valorText.setText(NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(proximoHorario.getValor()));
		}

		if (isProximoHorarioVisible) {
			// Proximo Horário
			FrameLayout proximoHorarioLayout = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_item_linha_info, null);
			proximoHorarioLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			TextView proximoHorarioText = (TextView) proximoHorarioLayout.findViewById(R.id.text);
			proximoHorarioText.setBackgroundResource(R.color.mark_gray);
			grid.addView(proximoHorarioLayout);

			// Proximo Horário & Saída
			if (!horarioSaida.isAfter(LocalTime.now())) {
				proximoHorarioLayout.setVisibility(View.GONE);
			} else {
				// Próximo Horário
				String proximoHorarioTexto = String
						.format("<b>%s</b> %s", getString(R.string.proximo_horario_min), horarioSaida.toString("HH:mm'H'"));
				proximoHorarioText.setText(Html.fromHtml(proximoHorarioTexto));
			}
		}

		if (isSaidaVisible) {
			// Saída
			FrameLayout saidaEm = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_item_linha_info, null);
			saidaEm.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			TextView saidaEmText = (TextView) saidaEm.findViewById(R.id.text);
			saidaEmText.setBackgroundResource(R.color.mark_blue);
			grid.addView(saidaEm);

			// Proximo Horário & Saída
			if (!horarioSaida.isAfter(LocalTime.now())) {
				saidaEm.setVisibility(View.GONE);
			} else {
				// Horário Saída
				int tempoEspera = proximoHorario.getTempoDeEspera();

				if (tempoEspera < 2)
					saidaEmText.setText(getString(R.string.format_saida_agora));
				else if (tempoEspera < 60)
					saidaEmText.setText(String.format(getString(R.string.format_saida_em), tempoEspera));
				else
					saidaEm.setVisibility(View.GONE);
			}
		}

		if (isAlarmeVisible) {
			// TODO: Adicionar novo alarme
			FrameLayout alarme = (FrameLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_item_linha_info, null);
			alarme.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			TextView alarmeText = (TextView) alarme.findViewById(R.id.text);
			alarmeText.setBackgroundResource(R.color.mark_red);
			alarmeText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_alarme_white, 0, 0, 0);
			grid.addView(alarme);
		}

//		if (linha.getEmpresa() != null) {
//			Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
//			Picasso.with(getActivity()).load(linha.getEmpresa().getLogo()).error(transparentDrawable).into(image);
//		}
	}

	public boolean isValorVisible() {
		return isValorVisible;
	}

	public void setValorVisible(boolean isValorVisible) {
		this.isValorVisible = isValorVisible;
	}

	public boolean isProximoHorarioVisible() {
		return isProximoHorarioVisible;
	}

	public void setProximoHorarioVisible(boolean isProximoHorarioVisible) {
		this.isProximoHorarioVisible = isProximoHorarioVisible;
	}

	public boolean isSaidaVisible() {
		return isSaidaVisible;
	}

	public void setSaidaVisible(boolean isSaidaVisible) {
		this.isSaidaVisible = isSaidaVisible;
	}

	public boolean isAlarmeVisible() {
		return isAlarmeVisible;
	}

	public void setAlarmeVisible(boolean isAlarmeVisible) {
		this.isAlarmeVisible = isAlarmeVisible;
	}

}
