package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.ui.DialogLinha;
import br.srv.rcs.onibus.ui.DialogLinha.DialogLinhaListener;
import br.srv.rcs.onibus.ui.Item;
import br.srv.rcs.onibus.ui.StretchedListView;
import br.srv.rcs.onibus.ui.StretchedListView.StretchedListViewListener;
import br.srv.rcs.onibus.ui.adapter.AdapterLinha;

import com.kanak.emptylayout.EmptyLayout;

@EFragment(R.layout.fragment_home)
public class HomeFragment extends BaseFragment implements StretchedListViewListener, ServiceListener {

	@ViewById
	protected FrameLayout frameEmpty;

	@ViewById
	protected StretchedListView linearMinhasLinhas;

	@ViewById
	protected RelativeLayout relUltimasBuscas;

	@ViewById
	protected StretchedListView linearUltimasBuscas;

	@Bean
	protected OnibusService onibusService;

	EmptyLayout mEmptyLayout;

	@Override
	protected void load() {
		super.load();

		mEmptyLayout = new EmptyLayout(getActivity(), frameEmpty);

		carregarMinhasLinhas();

		// Linhas Visualizadas Recentemente (que não seja favorita)
		carregarUltimasBuscas();

	}

	@Override
	protected void configureActionBar() {
		// TODO Auto-generated method stub
		super.configureActionBar();
		getActivity().setTitle(R.string.app_name);
	}

	@Background
	protected void carregarMinhasLinhas() {

		onibusService.buscarMinhasLinhas(new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(final RequestResult result) {

				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (!result.isError()) {
							@SuppressWarnings("unchecked")
							List<Linha> linhas = (List<Linha>) result.getData();
							carregarLinhas(linhas);

							if (linhas.isEmpty()) {
								mEmptyLayout.showEmpty();
								frameEmpty.setVisibility(View.VISIBLE);
							}
							// txtEmptyMinhasLinhas.setVisibility(View.VISIBLE);

						} else {
							mEmptyLayout.showEmpty();
							frameEmpty.setVisibility(View.VISIBLE);
						}
						// txtEmptyMinhasLinhas.setVisibility(View.VISIBLE);
					}
				});

			}
		});
	}

	@Background
	protected void carregarUltimasBuscas() {

		onibusService.buscarUltimasLinhasVisualizadas(new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(final RequestResult result) {

				if (!result.isError()) {
					@SuppressWarnings("unchecked")
					List<Linha> linhas = (List<Linha>) result.getData();
					carregarUltimasLinhasVisualizadas(linhas);
				}
			}
		});
	}

	@UiThread
	protected void carregarLinhas(List<Linha> linhas) {

		if (linhas.size() > 0) {
			AdapterLinha adapter = new AdapterLinha(getActivity(), linhas);
			linearMinhasLinhas.setAdapter(adapter);
			linearMinhasLinhas.setListener(this);
			frameEmpty.setVisibility(View.GONE);
		} else {
			linearMinhasLinhas.setAdapter(null);
			showEmpty(mEmptyLayout);
			frameEmpty.setVisibility(View.VISIBLE);
		}
	}

	@UiThread
	protected void carregarUltimasLinhasVisualizadas(List<Linha> linhas) {

		if (linhas.size() > 0) {
			relUltimasBuscas.setVisibility(View.VISIBLE);
			AdapterLinha adapter = new AdapterLinha(getActivity(), linhas);
			linearUltimasBuscas.setAdapter(adapter);
			linearUltimasBuscas.setListener(this);
		} else {
			relUltimasBuscas.setVisibility(View.GONE);
			linearUltimasBuscas.setAdapter(null);
		}
	}

	@Override
	public void onItemClick(ListAdapter adapter, int position) {
		Linha linhaOnibus = (Linha) adapter.getItem(position);
		DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);
	}

	@Override
	public void onItemLongClick(ListAdapter adapter, int position) {

		final Linha linhaOnibus = (Linha) adapter.getItem(position);

		List<Item> items = new ArrayList<Item>();
		items.add(new Item(1, getString(R.string.dialog_visualizar_detalhe)));
		items.add(new Item(2, getString(R.string.dialog_agendar_add)));
		if (linhaOnibus.isFavorito())
			items.add(new Item(3, getString(R.string.dialog_favoritos_del)));
		else
			items.add(new Item(3, getString(R.string.dialog_favoritos_add)));

		DialogLinha dialog = new DialogLinha(linhaOnibus.getNome(), items, new DialogLinhaListener() {

			@Override
			public void onItemClick(Item item) {
				if (item.getId() == 1)
					DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

				if (item.getId() == 2)
					AlarmeActivity_.intent(getActivity()).linhaOnibus(linhaOnibus).startForResult(2);

				if (item.getId() == 3)
					favoritarLinha(linhaOnibus);

			}
		});
		dialog.show(getFragmentManager(), DialogLinha.class.getSimpleName());

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1) {
				load();
			}
			if (requestCode == 2) {
				load();
				Toast.makeText(getActivity(), R.string.alarme_criado, Toast.LENGTH_SHORT).show();
			}
		}
	}

	@UiThread
	@Click(R.id.btnUltimasBuscasLimpar)
	protected void limparTodasAsBuscas() {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder = builder.setMessage(R.string.excluir_linha);
		builder.setTitle(R.string.ultimas_buscas);
		builder = builder.setPositiveButton(R.string.sim, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				onibusService.limparTodasAsBuscas(HomeFragment.this);
				showToast(R.string.ultimas_buscas_limpadas);
			}
		});

		builder = builder.setNegativeButton(R.string.cancelar, null);

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@Background
	protected void favoritarLinha(Linha linha) {
		onibusService.favoritarLinha(linha.getCodigo(), this);

		int texto = linha.isFavorito() ? R.string.linha_favorito_removido : R.string.linha_favorito_adicionada;
		showToast(texto);
	}

	@Override
	public void onPreInit() {
		// TODO Auto-generated method stub

	}

	@Override
	@UiThread
	public void onPostExecute(RequestResult result) {
		if (!result.isError())
			load();

	}

}
