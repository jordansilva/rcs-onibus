package br.srv.rcs.onibus;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.helper.Constants.DiaOnibus;
import br.srv.rcs.onibus.helper.HorarioHelper;
import br.srv.rcs.onibus.ui.DialogLinha;
import br.srv.rcs.onibus.ui.DialogLinha.DialogLinhaListener;
import br.srv.rcs.onibus.ui.ExtedendButton;
import br.srv.rcs.onibus.ui.Item;
import br.srv.rcs.onibus.ui.adapter.AdapterDetalheHorario.AdapterDetalheHorarioListener;
import br.srv.rcs.onibus.ui.adapter.PagerAdapterHorario;

import com.astuetz.PagerSlidingTabStrip;
import com.squareup.picasso.Picasso;

@EActivity(R.layout.activity_detalhe_linha)
public class DetalheLinhaActivity extends BaseActivity implements AdapterDetalheHorarioListener {

	private Linha mLinha;
	private MenuItem menuFavorito;
	private PagerAdapterHorario adapter;

	@Extra
	protected String codigoLinha;

	@ViewById
	protected TextView txtLinha;

	@ViewById
	protected ImageView imgLinha;

	@ViewById
	protected TextView txtValor;

	@ViewById
	protected TextView txtProximoHorario;

	@ViewById
	protected TextView txtAlarme;

	@ViewById
	protected PagerSlidingTabStrip tabs;

	@ViewById
	protected ViewPager pager;

	@ViewById
	protected ExtedendButton btnLegenda;

	@ViewById
	protected ExtedendButton btnInverter;

	@Bean
	OnibusService service;

	@Override
	protected void load() {
		super.load();
		if (codigoLinha != null && !codigoLinha.isEmpty())
			buscarLinha(codigoLinha);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);

		Bundle bundle = intent.getExtras();
		if (bundle != null && bundle.containsKey("codigoLinha")) {
			codigoLinha = bundle.getString("codigoLinha");
			load();
		}
	}

	protected void buscarLinha(final String codigo) {

		service.buscarLinha(codigo, new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					codigoLinha = codigo;
					mLinha = (Linha) result.getData();
					registrarUltimaVisualizacao();
					configurarUi();
				} else
					showError();

			}
		});
	}

	@Background
	protected void configurarUi() {
		configurarHorarioAdapter();
		configurarHorarios();
		configurarTexto();
		configurarAlarmes();
		configurarSublinhas();
		configurarMenuFavorito();

	}

	protected void configurarHorarioAdapter() {
		Collection<Horario> semana = mLinha.getHorarios(DiaOnibus.SEMANA);
		Collection<Horario> sabado = mLinha.getHorarios(DiaOnibus.SABADO);
		Collection<Horario> domingo = mLinha.getHorarios(DiaOnibus.DOMINGO);

		adapter = new PagerAdapterHorario(getSupportFragmentManager());

		int diaAtual = LocalDateTime.now().getDayOfWeek();

		if (semana != null && semana.size() > 0)
			adapter.addFragment(getString(R.string.tab_dia_semana), DetalheHorarioFragment_.builder().horarios((ArrayList<Horario>) semana).build(),
					(diaAtual <= 5));
		if (sabado != null && sabado.size() > 0)
			adapter.addFragment(getString(R.string.tab_sabado), DetalheHorarioFragment_.builder().horarios((ArrayList<Horario>) sabado).build(),
					(diaAtual == 6));
		if (domingo != null && domingo.size() > 0)
			adapter.addFragment(getString(R.string.tab_domingo), DetalheHorarioFragment_.builder().horarios((ArrayList<Horario>) domingo).build(),
					(diaAtual == 7));

	}

	@UiThread
	protected void configurarHorarios() {

		pager.setAdapter(adapter);
		pager.setCurrentItem(adapter.getPosicaoInicial());

		tabs.setShouldExpand(true);
		tabs.setTextColorResource(R.color.gray);
		tabs.setViewPager(pager);
		adapter.moverParaHoraAtual(pager.getCurrentItem());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_detalhe_linha, menu);
		menuFavorito = menu.findItem(R.id.action_favorito);

		configurarMenuFavorito();

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_favorito:
			favoritar();
		}

		return super.onOptionsItemSelected(item);
	}

	@UiThread
	protected void configurarTexto() {
		setTitle(mLinha.toString());

		Horario proximoHorario = mLinha.getProximoHorario();

		txtLinha.setText(mLinha.getNome());

		Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
		Picasso.with(this).load(mLinha.getEmpresa().getLogo()).error(transparentDrawable).into(imgLinha);

		txtValor.setText(NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(proximoHorario.getValor()));

		if (proximoHorario.getHorarioSaida().toLocalTime().isAfter(LocalTime.now())) {
			String proximoHorarioTexto = String.format("%s %s", getString(R.string.proximo_horario_min),
					proximoHorario.getHorarioSaida().toString("HH:mm'H'"));

			int tempoEspera = proximoHorario.getTempoDeEspera();
			String textoTempoEspera = "";
			if (tempoEspera < 2)
				textoTempoEspera = getString(R.string.format_saida_agora);
			else if (tempoEspera < 60)
				textoTempoEspera = String.format(getString(R.string.format_tempo_espera), tempoEspera);

			if (!textoTempoEspera.isEmpty())
				proximoHorarioTexto = proximoHorarioTexto.concat(" (" + textoTempoEspera + ")");

			txtProximoHorario.setText(proximoHorarioTexto.toUpperCase(Locale.getDefault()));
		} else
			txtProximoHorario.setVisibility(View.GONE);

	}

	@UiThread
	protected void configurarAlarmes() {
		List<Alarme> alarmes = mLinha.getAlarmesDoDia();
		if (alarmes.size() > 0) {
			Collections.sort(alarmes, new Comparator<Alarme>() {

				@Override
				public int compare(Alarme lhs, Alarme rhs) {
					return lhs.getHorario().compareTo(rhs.getHorario());
				}
			});

			String[] alarmesDoDia = new String[alarmes.size()];

			for (int i = 0; i < alarmes.size(); i++) {
				alarmesDoDia[i] = alarmes.get(i).getHorario().toString("HH:mm");
			}

			txtAlarme.setText(TextUtils.join(", ", alarmesDoDia));
			txtAlarme.setVisibility(View.VISIBLE);
		} else
			txtAlarme.setVisibility(View.GONE);
	}

	@SuppressLint("InflateParams")
	@UiThread
	protected void configurarSublinhas() {
		final List<String> legendas = HorarioHelper.buscarLegendas(mLinha.getHorarios());
		if (legendas == null || legendas.size() == 0)
			btnLegenda.setVisibility(View.GONE);
		else {
			btnLegenda.setVisibility(View.VISIBLE);
			btnLegenda.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					LegendaLinhaFragment dialog = new LegendaLinhaFragment(getString(R.string.legenda), legendas);
					dialog.show(getSupportFragmentManager(), LegendaLinhaFragment.class.getSimpleName());
				}
			});
		}
	}

	@Background
	protected void favoritar() {
		service.favoritarLinha(mLinha.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(final RequestResult result) {

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (!result.isError()) {
							setResult(RESULT_OK);
							mLinha.setFavorito(!mLinha.isFavorito());
							int texto = (mLinha.isFavorito()) ? R.string.linha_favorito_adicionada : R.string.linha_favorito_removido;
							Toast.makeText(DetalheLinhaActivity.this, texto, Toast.LENGTH_LONG).show();
							configurarMenuFavorito();
						}
					}
				});
			}
		});
	}

	@UiThread
	protected void configurarMenuFavorito() {

		int image = R.drawable.ico_menu_star_colored;
		if (mLinha != null && !mLinha.isFavorito())
			image = R.drawable.ico_menu_star;

		if (menuFavorito != null)
			menuFavorito.setIcon(image);
	}

	@Background
	protected void registrarUltimaVisualizacao() {
		if (mLinha != null) {
			mLinha.setUltimaVisualizacao(DateTime.now());
			service.registrarUltimaVisualizacao(mLinha.getCodigo(), DateTime.now(), new ServiceListener() {

				@Override
				public void onPreInit() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPostExecute(RequestResult result) {
					// TODO Auto-generated method stub

				}

			});
		}
	}

	@Click(R.id.btnInverter)
	protected void inverterLinha() {
		if (mLinha != null) {
			String codigoInvertido = mLinha.getCodigoLinhaInverso();
			buscarLinha(codigoInvertido);
		}
	}

	@UiThread
	protected void showError() {
		Toast.makeText(this, R.string.linha_nao_encontrada, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mLinha = (Linha) savedInstanceState.get("linhaOnibus");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("linhaOnibus", mLinha);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			if (requestCode == 2) {
				Toast.makeText(this, R.string.alarme_criado, Toast.LENGTH_SHORT).show();
				buscarLinha(codigoLinha);
			}

			if (requestCode == 1)
				buscarLinha(codigoLinha);

			setResult(resultCode);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void horarioOnClick(final Horario horario) {

		List<Item> items = new ArrayList<Item>();
		items.add(new Item(1, getString(R.string.dialog_visualizar_itinerario)));
		items.add(new Item(2, getString(R.string.dialog_agendar_add)));

		final DateTime horarioSaida = horario.getHorarioSaida();

		String title = String.format("%s - ", horarioSaida.toString("HH:mm"));

		if (horario.getDescricao() != null && !horario.getDescricao().isEmpty())
			title += horario.getDescricao();
		else
			title += mLinha.getNome();

		DialogLinha dialog = new DialogLinha(title, items, new DialogLinhaListener() {

			@Override
			public void onItemClick(Item item) {
				if (item.getId() == 1)
					ItinerarioActivity_.intent(DetalheLinhaActivity.this).linhaOnibus(mLinha).horarioOnibus(horario).startForResult(1);

				if (item.getId() == 2)
					AlarmeActivity_.intent(DetalheLinhaActivity.this).linhaOnibus(mLinha).horarioOnibus(horario).startForResult(2);

			}
		});

		dialog.show(getSupportFragmentManager(), DialogLinha.class.getSimpleName());
	}

	@Override
	public void horarioOnPressed(Horario horario) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onResume() {
		if (pager != null && pager.getAdapter() == null && adapter != null)
			pager.setAdapter(adapter);

		super.onResume();
	}

	@Override
	protected void onPause() {
		if (pager != null)
			pager.setAdapter(null);

		super.onPause();
	}

}
