package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.ui.DialogLinha;
import br.srv.rcs.onibus.ui.DialogLinha.DialogLinhaListener;
import br.srv.rcs.onibus.ui.Item;
import br.srv.rcs.onibus.ui.adapter.AdapterLinha;

import com.kanak.emptylayout.EmptyLayout;

@EActivity(R.layout.activity_todas_linhas)
public class TodasLinhasActivity extends BaseActivity implements OnItemClickListener, OnItemLongClickListener {

	private EmptyLayout mEmptyMinhasLinhas;

	@ViewById
	protected ListView listView;

	@Bean
	protected OnibusService onibusService;

	@Override
	protected void load() {
		super.load();

		mEmptyMinhasLinhas = new EmptyLayout(TodasLinhasActivity.this, listView);
		carregarMinhasLinhas();
	}

	@Background
	protected void carregarMinhasLinhas() {

		onibusService.listarTodasAsLinhas(new ServiceListener() {

			@Override
			public void onPreInit() {
				showLoading(mEmptyMinhasLinhas);
			}

			@Override
			public void onPostExecute(final RequestResult result) {

				if (!result.isError()) {
					@SuppressWarnings("unchecked")
					List<Linha> linhas = (List<Linha>) result.getData();
					if (!linhas.isEmpty())
						carregarLinhas(linhas);
					else
						showEmpty(mEmptyMinhasLinhas);
				} else
					showError(mEmptyMinhasLinhas);
			}
		});
	}

	@UiThread
	protected void carregarLinhas(List<Linha> linhas) {
		listView.setAdapter(new AdapterLinha(TodasLinhasActivity.this, linhas, R.layout.fragment_item_linha_list));
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);
		DetalheLinhaActivity_.intent(TodasLinhasActivity.this).codigoLinha(linhaOnibus.getCodigo()).start();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

		final Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);

		List<Item> items = new ArrayList<Item>();
		items.add(new Item(1, getString(R.string.dialog_visualizar_detalhe)));
		items.add(new Item(2, getString(R.string.dialog_agendar_add)));
		if (linhaOnibus.isFavorito())
			items.add(new Item(3, getString(R.string.dialog_favoritos_del)));
		else
			items.add(new Item(3, getString(R.string.dialog_favoritos_add)));

		DialogLinha dialog = new DialogLinha(linhaOnibus.getNome(), items, new DialogLinhaListener() {

			@Override
			public void onItemClick(Item item) {
				if (item.getId() == 1)
					DetalheLinhaActivity_.intent(TodasLinhasActivity.this).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

				if (item.getId() == 2)
					AlarmeActivity_.intent(TodasLinhasActivity.this).linhaOnibus(linhaOnibus).startForResult(2);

				if (item.getId() == 3)
					favoritarLinha(linhaOnibus);

			}
		});
		dialog.show(getSupportFragmentManager(), DialogLinha.class.getSimpleName());

		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1) {
				carregarMinhasLinhas();
			}
			if (requestCode == 2) {
				carregarMinhasLinhas();
				Toast.makeText(this, R.string.alarme_criado, Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Background
	protected void favoritarLinha(final Linha linha) {
		onibusService.favoritarLinha(linha.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							int texto = linha.isFavorito() ? R.string.linha_favorito_removido : R.string.linha_favorito_adicionada;
							Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT).show();
						}
					});

				}
			}
		});

	}
}
