package br.srv.rcs.onibus;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;

import com.kanak.emptylayout.EmptyLayout;

@EActivity
public abstract class BaseActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	protected void load() {
		configureActionBar();
		configureBackground();
	}

	protected void configureBackground() {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)
		{
			View v = getWindow().getDecorView().findViewById(android.R.id.content);
			v.setBackgroundColor(Color.parseColor("#fff3f3f3"));
		}
	}
	
	@Override
	public void setTitle(int titleId) {
		super.setTitle(titleId);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null)
			actionBar.setTitle(titleId);
	}
	
	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null)
			actionBar.setTitle(title);
	}

	public void configureActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayUseLogoEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@UiThread
	protected void showEmpty(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showEmpty();
	}

	@UiThread
	protected void showLoading(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showLoading();
	}

	@UiThread
	protected void showError(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showError();
	}
}
