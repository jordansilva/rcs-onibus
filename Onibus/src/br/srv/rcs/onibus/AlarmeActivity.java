package br.srv.rcs.onibus;

import java.util.Calendar;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import br.srv.rcs.onibus.business.AlarmeService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Linha;

import com.android.colorpicker.ColorPickerDialog;
import com.android.colorpicker.ColorPickerSwatch.OnColorSelectedListener;

@EActivity(R.layout.activity_alarme)
public class AlarmeActivity extends BaseActivity {

	private Alarme alarme;
	private int[] cores;

	@Extra
	protected Linha linhaOnibus;

	@Extra
	protected Horario horarioOnibus;

	@FragmentById
	protected LinhaItemFragment_ linhaFragment;

	@ViewById
	protected LinearLayout lnlRepetirSemanalmente;

	@ViewById
	protected LinearLayout lnlDia;

	@ViewById
	protected TextView txtDia;

	@ViewById
	protected TextView txtHora;

	@ViewById
	protected FrameLayout frameColor;

	@Bean
	protected AlarmeService service;

	@Override
	protected void load() {
		super.load();

		construirAlarme();
		linhaFragment.load();
	}

	@Override
	public void onAttachFragment(android.support.v4.app.Fragment fragment) {
		super.onAttachFragment(fragment);

		if (fragment instanceof LinhaItemFragment_) {
			linhaFragment = (LinhaItemFragment_) fragment;
			linhaFragment.linha = linhaOnibus;
			linhaFragment.setAlarmeVisible(false);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_alarme, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_salvar:
			salvarAlarme();
		}

		return super.onOptionsItemSelected(item);
	}

	private void construirAlarme() {
		if (alarme == null) {

			String[] colors = getResources().getStringArray(R.array.default_color_choice_values);
			cores = new int[colors.length];
			for (int i = 0; i < colors.length; i++) {
				cores[i] = Color.parseColor(colors[i]);
			}

			alarme = new Alarme();
			alarme.setData(LocalDate.now());
			alarme.setLinha(linhaOnibus);
			alarme.setCor(cores[0]);

			if (horarioOnibus != null) {
				LocalTime horario = horarioOnibus.getHorarioSaida().toLocalTime();
				alarme.setHorario(horario);
			} else
				alarme.setHorario(LocalTime.now());

		}
		String text = alarme.getData().toString("dd/MM/yy");
		if (alarme.getData().isEqual(LocalDate.now()))
			text = getString(R.string.alarme_hoje);

		txtDia.setText(text);
		txtHora.setText(alarme.getHorario().toString("HH:mm"));
		frameColor.setBackgroundColor(alarme.getCor());
	}

	@Click(R.id.frameColor)
	protected void clickFrameColor(View v) {
		ColorPickerDialog dialog = ColorPickerDialog.newInstance(R.string.color_picker_default_title, cores, alarme.getCor(), 4, 2);
		dialog.setOnColorSelectedListener(new OnColorSelectedListener() {

			@Override
			public void onColorSelected(int color) {
				alarme.setCor(color);
				frameColor.setBackgroundColor(color);
			}
		});
		dialog.show(getSupportFragmentManager(), "COLOR");
	}

	@CheckedChange(R.id.chkRepetirSemanalmente)
	protected void checkRepetirSemanalmente(CompoundButton v, boolean isChecked) {

		alarme.setRepetirSemanalmente(true);

		if (isChecked)
			lnlRepetirSemanalmente.setVisibility(View.VISIBLE);
		else
			lnlRepetirSemanalmente.setVisibility(View.GONE);
	}

	@CheckedChange({ R.id.chkSegunda, R.id.chkTerca, R.id.chkQuarta, R.id.chkQuinta, R.id.chkSexta, R.id.chkSabado, R.id.chkDomingo })
	protected void checkDias(CompoundButton v, boolean isChecked) {
		int dia = getDiaDaSemana(v.getId());
		if (dia != 0) {
			if (isChecked)
				alarme.adicionarDia(dia);
			else
				alarme.removerDia(dia);
		}
	}

	private int getDiaDaSemana(int id) {
		switch (id) {
		case R.id.chkSegunda:
			return Calendar.MONDAY;
		case R.id.chkTerca:
			return Calendar.TUESDAY;
		case R.id.chkQuarta:
			return Calendar.WEDNESDAY;
		case R.id.chkQuinta:
			return Calendar.THURSDAY;
		case R.id.chkSexta:
			return Calendar.FRIDAY;
		case R.id.chkSabado:
			return Calendar.SATURDAY;
		case R.id.chkDomingo:
			return Calendar.SUNDAY;
		default:
			return 0;
		}
	}

	@Click(R.id.lnlDia)
	protected void clickDia() {

		DatePickerDialog dialog = new DatePickerDialog(this, new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				LocalDate localDate = new LocalDate(year, monthOfYear + 1, dayOfMonth);
				alarme.setData(localDate);
				construirAlarme();
			}

		}, alarme.getData().getYear(), alarme.getData().getMonthOfYear() - 1, alarme.getData().getDayOfMonth());

		dialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {

			}
		});

		dialog.show();
	}

	@Click(R.id.lnlHora)
	protected void clickHora() {

		LocalTime hora = alarme.getHorario();
		TimePickerDialog dialog = new TimePickerDialog(this, new OnTimeSetListener() {

			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				LocalTime localTime = new LocalTime(hourOfDay, minute);
				alarme.setHorario(localTime);
				construirAlarme();
			}
		}, hora.getHourOfDay(), hora.getMinuteOfHour(), true);

		dialog.show();
	}

	@Background
	protected void salvarAlarme() {
		
		if (alarme != null && alarme.isRepetirSemanalmente() && alarme.getRepetirDias().isEmpty())
			alarme.setRepetirSemanalmente(false);
		
		service.salvarAlarme(alarme, new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(final RequestResult result) {

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (!result.isError()) {
							// criarAlarme();
							setResult(Activity.RESULT_OK);
							finish();
						} else
							setResult(Activity.RESULT_CANCELED);
					}
				});

			}
		});
	}

}