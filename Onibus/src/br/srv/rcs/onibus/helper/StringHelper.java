package br.srv.rcs.onibus.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import android.annotation.SuppressLint;
import android.content.Context;

public class StringHelper {
	private static SimpleDateFormat format = new SimpleDateFormat("dd MMM hh:mm'hs'", Locale.getDefault());

	public static boolean isNullOrEmpty(String text) {
		return text == null || text.trim().length() == 0;
	}

	public static boolean isNullOrWhiteSpace(String value) {
		return value == null || value.trim().length() == 0;
	}

	public static HashMap<String, String> mapperORMRawRowMapper(String[] columnNames, String[] resultColumns) {
		HashMap<String, String> result = new HashMap<String, String>();

		for (int i = 0; i < columnNames.length; i++)
			result.put(columnNames[i], resultColumns[i]);

		return result;

	}

	@SuppressLint("DefaultLocale")
	public static String capitalize(String source) {
		try {
			String[] tokens = source.toLowerCase(Locale.getDefault()).split("\\s");
			String text = "";

			for (int i = 0; i < tokens.length; i++) {
				char capLetter = Character.toUpperCase(tokens[i].charAt(0));
				text += " " + capLetter + tokens[i].substring(1, tokens[i].length());
			}
			return text;
		} catch (Exception ex) {
		}

		return source;
	}

	public static String getStringResourceByName(Context context, String name) {
		int resId = context.getResources().getIdentifier(name, "string", context.getPackageName());
		return context.getString(resId);
	}

	public static String formatDate(DateTime date) {
		return format.format(date.toDate());
	}

	public static String formatDate(DateTime date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
		return sdf.format(date.toDate());
	}

	public static String formatDayOfWeek(int day) {
		switch (day) {
		case Calendar.SUNDAY:
			return "DOM";
		case Calendar.MONDAY:
			return "SEG";
		case Calendar.TUESDAY:
			return "TER";
		case Calendar.WEDNESDAY:
			return "QUA";
		case Calendar.THURSDAY:
			return "QUI";
		case Calendar.FRIDAY:
			return "SEX";
		case Calendar.SATURDAY:
			return "SAB";
		default:
			return null;
		}
	}
}
