package br.srv.rcs.onibus.helper;

public class Constants {

	public static final String URL_RCS = "http://www.rcs.srv.br";
	public static final String EMAIL_CONTATO = "atendimento.sistemas@rcs.srv.br";

	public enum DiaOnibus {
		SEMANA(1), SABADO(2), DOMINGO(3), FERIADO(3);

		public int diaOnibus;

		private DiaOnibus(int valor) {
			this.diaOnibus = valor;
		}
	}
}
