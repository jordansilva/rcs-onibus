package br.srv.rcs.onibus.helper;

import org.androidannotations.annotations.EBean;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

@EBean
public class GoogleMapsHelper {

	private Context context;

	public GoogleMapsHelper(Context context) {
		this.context = context;
	}

	@SuppressWarnings("unused")
	public boolean isGoogleMapsInstalled() {
		try {
			ApplicationInfo info = context.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

}
