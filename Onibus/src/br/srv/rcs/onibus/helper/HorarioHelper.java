package br.srv.rcs.onibus.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.joda.time.LocalTime;

import br.srv.rcs.onibus.business.domain.Horario;

public class HorarioHelper {

	public static Horario buscarProximoHorario(Collection<Horario> horarios) {

		Horario proximoHorario = null;

		if (horarios != null && horarios.size() > 0) {
			ArrayList<Horario> sortedHorarios = new ArrayList<Horario>(horarios);
			Collections.sort(sortedHorarios, new Comparator<Horario>() {

				@Override
				public int compare(Horario lhs, Horario rhs) {
					LocalTime lhsTime = lhs.getHorarioSaida().toLocalTime();
					LocalTime rhsTime = rhs.getHorarioSaida().toLocalTime();

					return lhsTime.compareTo(rhsTime);
				}
			});

			LocalTime proximoHorarioTime = null;
			LocalTime horarioTime = null;

			for (Horario h : sortedHorarios) {
				if (h.funcionaHoje()) {
					horarioTime = h.getHorarioSaida().toLocalTime();

					if (proximoHorario == null) {
						proximoHorario = h;
						proximoHorarioTime = horarioTime;

					} else if (horarioTime.isAfter(LocalTime.now()) && horarioTime.isBefore(proximoHorarioTime)
							|| (proximoHorarioTime.isBefore(LocalTime.now()) && horarioTime.isAfter(proximoHorarioTime))) {
						proximoHorario = h;
						proximoHorarioTime = horarioTime;
					}
				}
			}

			if (proximoHorario == null)
				if (horarios.size() > 0)
					proximoHorario = (Horario) horarios.toArray()[0];
				else
					proximoHorario = null;

		}

		return proximoHorario;
	}
	
	public static List<String> buscarLegendas(Collection<Horario> horarios)
	{
		ArrayList<String> legendas = new ArrayList<String>();
		
		for (Horario horario : horarios) {
			if (horario.getDescricao() != null && !horario.getDescricao().isEmpty()) {
				String sublinha = horario.getDescricao().trim().toUpperCase(Locale.getDefault());
				if (!legendas.contains(sublinha))
					legendas.add(sublinha);
			}
		}
		
		return legendas;
	}

}
