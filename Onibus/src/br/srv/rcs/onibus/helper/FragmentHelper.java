package br.srv.rcs.onibus.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import br.srv.rcs.onibus.DrawerActivity;
import br.srv.rcs.onibus.MenuFragment;
import br.srv.rcs.onibus.PropagandaFragment;
import br.srv.rcs.onibus.R;

/**
 * @author jordansilva
 * 
 */
@EBean(scope = Scope.Singleton)
public class FragmentHelper {

	private static Fragment mCurrentFragment;
	private static ArrayList<UUID> mFragments;
	private static FragmentManager mFragmentManager;

	@RootContext
	Context context;

	@RootContext
	DrawerActivity activity;

	@AfterInject
	protected void init() {
		mFragments = new ArrayList<UUID>();
	}

	public void moveToFragment(Fragment fragment) {
		moveToFragment(fragment, true);
	}

	public boolean backToFragment() {
		if (mFragments.size() > 0)
			mFragments.remove(mFragments.size() - 1);

		if (mFragmentManager.getBackStackEntryCount() > 1) {
			mFragmentManager.popBackStack();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param fragment
	 * @param addToBackStack
	 */
	public void moveToFragment(Fragment fragment, boolean addToBackStack) {
		if (fragment != null) {
			// Creating a fragment transaction
			FragmentTransaction ft = mFragmentManager.beginTransaction();

			String id = UUID.randomUUID().toString();
			// Adding a fragment to the fragment transaction
			ft.replace(R.id.content_frame, fragment, id);

			if (addToBackStack)
				ft.addToBackStack(id);

			// Committing the transaction

			ft.commit();

			mFragments.add(UUID.randomUUID());

			mCurrentFragment = fragment;
			mFragmentManager.executePendingTransactions();
		}
	}

	public Fragment getCurrentFragment() {
		List<Fragment> fragments = mFragmentManager.getFragments();
		for (Fragment fragment : fragments) {
			if (fragment != null && fragment.isVisible() && !(fragment instanceof MenuFragment) && !(fragment instanceof PropagandaFragment)) {
				mCurrentFragment = fragment;
				return mCurrentFragment;
			}
		}
		return null;
	}

	public boolean isLastFragment() {
		return (mFragments.size() == 0);
	}

	public void setSupportFragmentManager(FragmentManager supportFragmentManager) {
		mFragmentManager = supportFragmentManager;
	}
}
