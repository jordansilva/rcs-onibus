package br.srv.rcs.onibus;

import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import br.srv.rcs.onibus.ui.ExtedendTextView;

public class LegendaLinhaFragment extends DialogFragment {

	private String mTitle;
	private List<String> mList;
	private View mView;

	public LegendaLinhaFragment(String title, List<String> list) {
		mTitle = title;
		mList = list;

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		carregarLista(mList);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder = builder.setTitle(mTitle);
		builder.setCancelable(true);
		builder.setView(mView);
		builder.setInverseBackgroundForced(true);
		Dialog dialog = builder.create();
		
		return dialog;
	}

	private void carregarLista(List<String> legendas) {

		int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());

		LinearLayout linearLayout = new LinearLayout(getActivity());
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		//linearLayout.setBackgroundColor(Color.WHITE);
		linearLayout.setPadding(padding, padding, padding, padding);
		linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		for (int i = 0; i < legendas.size(); i++) {
			ExtedendTextView v = (ExtedendTextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_extendedtextview, null);
			String l = String.format(Locale.getDefault(), "(%d) %s", i + 1, legendas.get(i));
			v.setText(l);
			linearLayout.addView(v);
		}

		mView = linearLayout;
	}

	@Override
	public void onPause() {
		dismiss();
		super.onPause();
	}
}
