package br.srv.rcs.onibus;

import java.util.List;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import br.srv.rcs.onibus.MenuFragment.MenuListener;
import br.srv.rcs.onibus.helper.FragmentHelper;

import com.crashlytics.android.Crashlytics;

@EActivity(R.layout.nav_drawer)
public class DrawerActivity extends BaseActivity implements MenuListener {

	@ViewById(R.id.drawer_layout)
	DrawerLayout mDrawerLayout;

	@ViewById(R.id.drawer)
	FrameLayout mDrawerFrame;

	@ViewById(R.id.content_frame)
	FrameLayout mContentFrame;

	@FragmentById(R.id.publicidade)
	PropagandaFragment_ publicidade;

	@FragmentById(R.id.menuFragment)
	MenuFragment_ menuFragment;

	@Bean
	FragmentHelper mFragmentHelper;

	private ActionBarDrawerToggle mDrawerToggle;
	private boolean mIsDrawerLocked;
	private static boolean isFirstTime = true;

	// Title of the action bar
	private String mTitle = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		super.load();
		mFragmentHelper.setSupportFragmentManager(getSupportFragmentManager());
		carregarActionBar();
		// carregarMenu();
		carregarTelaInicial();
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title.toString();
		super.setTitle(title);
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	public void setTitle(int titleId) {
		mTitle = getString(titleId);
		super.setTitle(titleId);
		getSupportActionBar().setTitle(mTitle);
	}

	private void carregarActionBar() {

		if (!mIsDrawerLocked) {
			// Getting reference to the ActionBarDrawerToggle
			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open,
					R.string.drawer_close) {

				/** Called when drawer is closed */
				public void onDrawerClosed(View view) {
					getSupportActionBar().setTitle(mTitle);
					supportInvalidateOptionsMenu();
				}

				/** Called when a drawer is opened */
				public void onDrawerOpened(View drawerView) {
					getSupportActionBar().setTitle(R.string.app_name);
					supportInvalidateOptionsMenu();
				}
			};

			// Setting DrawerToggle on DrawerLayout
			mDrawerLayout.setDrawerListener(mDrawerToggle);
			mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		}
	}

	protected void carregarMenu() {
		if (menuFragment == null) {
			List<Fragment> fragments = getSupportFragmentManager().getFragments();
			for (Fragment f : fragments)
				if (f instanceof MenuFragment_)
					menuFragment = (MenuFragment_) f;
		}
	}

	protected void carregarTelaInicial() {
		if (isFirstTime) {
			mFragmentHelper.moveToFragment(HomeFragment_.builder().build(), false);
			isFirstTime = false;
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mDrawerToggle != null)
			mDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/** Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onMenuSelected(int codigoMenu, String nomeMenu) {
		BaseFragment fragment = null;
		switch (codigoMenu) {
		case 1:
			fragment = HomeFragment_.builder().build();
			break;
		case 2:
			fragment = LinhasFragment_.builder().build();
			break;
		case 3:
			fragment = AlarmesFragment_.builder().build();
			break;
		case 4:
			fragment = FavoritosFragment_.builder().build();
			break;
		case 5:
			fragment = FaleConoscoFragment_.builder().build();
			break;
		case 6:
			fragment = SobreFragment_.builder().build();
			break;
		default:
			break;
		}

		if (isAnotherFragment(fragment))
			mFragmentHelper.moveToFragment(fragment);

		// Closing the drawer
		if (!mIsDrawerLocked)
			mDrawerLayout.closeDrawer(mDrawerFrame);

		if (!isPropagandaVisible())
			setPropagandaVisible(true);

		hideKeyboard();
	}

	protected void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mDrawerFrame.getWindowToken(), 0);
	}

	protected boolean isAnotherFragment(BaseFragment fragment) {
		if (fragment == null)
			return false;
		else if (mFragmentHelper.getCurrentFragment() != null && mFragmentHelper.getClass().equals(fragment.getClass()))
			return false;
		else
			return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Fragment f = mFragmentHelper.getCurrentFragment();
		if (f != null)
			f.onActivityResult(requestCode, resultCode, data);
		// super.onActivityResult(requestCode, resultCode, data);
	}

	public boolean isPropagandaVisible() {
		if (publicidade != null)
			return publicidade.getView().getVisibility() == View.VISIBLE;
		else
			return true;
	}

	public void setPropagandaVisible(boolean isVisible) {
		if (publicidade == null)
			publicidade = (PropagandaFragment_) getSupportFragmentManager().findFragmentById(R.id.publicidade);

		if (publicidade != null)
			publicidade.getView().setVisibility((isVisible) ? View.VISIBLE : View.GONE);
	}

	public int getPropagandaHeight() {

		if (publicidade == null)
			publicidade = (PropagandaFragment_) getSupportFragmentManager().findFragmentById(R.id.publicidade);

		if (publicidade != null)
			return publicidade.getView().getHeight();
		else
			return 0;
	}

	@Override
	public void onBackPressed() {

		if (!mFragmentHelper.backToFragment()) {
			super.onBackPressed();

			if (mFragmentHelper.isLastFragment())
				isFirstTime = true;

		}
	}

}
