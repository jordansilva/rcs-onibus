package br.srv.rcs.onibus;

import java.io.File;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.os.Environment;
import android.webkit.WebView;

@EActivity(R.layout.activity_termos_uso)
public class TermosDeUsoActivity extends BaseActivity {

	@ViewById
	protected WebView webView;

	@Override
	protected void load() {
		super.load();
		carregarTermosDeUso();
	}

	protected void carregarTermosDeUso() {
		File lFile = new File(Environment.getExternalStorageDirectory() + "termos_de_uso.html");
		webView.loadUrl("file:///" + lFile.getAbsolutePath());
	}

}
