package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.LocalTime;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.widget.ListView;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.ui.adapter.AdapterDetalheHorario;
import br.srv.rcs.onibus.ui.adapter.AdapterDetalheHorario.AdapterDetalheHorarioListener;

@EFragment(R.layout.fragment_detalhe_horario)
public class DetalheHorarioFragment extends Fragment {

	@FragmentArg
	protected ArrayList<Horario> horarios;

	@ViewById
	protected ListView listView;

	protected SparseArray<List<Horario>> groups;
	private int horaAtual = -1;
	private AdapterDetalheHorarioListener mListener;

	@AfterViews
	public void load() {
		if (horarios != null && horarios.size() > 0) {
			if (groups == null || groups.size() == 0)
				configurarHorarios();
			carregarLinhas();
		} else {
			listView.setAdapter(null);
		}
	}

	@UiThread
	protected void carregarLinhas() {
		listView.setAdapter(new AdapterDetalheHorario(getActivity(), groups, mListener));
		if (horaAtual >= 0)
			moverParaHorario(horaAtual);
	}

	protected void configurarHorarios() {
		groups = new SparseArray<List<Horario>>();

		for (Horario h : horarios) {
			LocalTime time = h.getHorarioSaida().toLocalTime();
			int hourOfDay = time.getHourOfDay();

			if (groups.indexOfKey(hourOfDay) < 0)
				groups.put(hourOfDay, new ArrayList<Horario>());

			groups.get(hourOfDay).add(h);
		}
	}

	public void moverParaHoraAtual() {
		horaAtual = LocalTime.now().getHourOfDay();
		moverParaHorario(horaAtual);
	}

	private void moverParaHorario(int hora) {
		if (groups != null && groups.size() > 0) {
			int position = groups.indexOfKey(hora);
			if (position >= 0)
				listView.setSelection(position);
			else if (hora < 23)
				moverParaHorario(++hora);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mListener = (AdapterDetalheHorarioListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement AdapterDetalheHorarioListener.");
		}
	}

}
