package br.srv.rcs.onibus.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.joda.time.DateTime;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Empresa;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Itinerario;
import br.srv.rcs.onibus.business.domain.Linha;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

public class LinhaRepository extends RepositoryBase<Linha> {

	public LinhaRepository(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public void save(final Linha linha) throws SQLException {

		TransactionManager.callInTransaction(getDao().getConnectionSource(), new Callable<Void>() {
			public Void call() throws Exception {

				Linha linha2 = get(linha.getCodigo());
				if (linha2 != null) {
					linha.setUltimaVisualizacao(linha2.getUltimaVisualizacao());
					linha.setFavorito(linha2.isFavorito());
				}
				// Empresa
				// item.setCodigo(0);
				getDao(Empresa.class).createOrUpdate(linha.getEmpresa());

				// Linha
				linha.setOrigem(linha.getOrigem().trim());
				linha.setDestino(linha.getDestino().trim());

				getDao().createOrUpdate(linha);

				// Horarios
				for (Horario h : linha.getHorarios()) {
					// h.setCodigo(0);
					h.setLinha(linha);
					getDao(Horario.class).createOrUpdate(h);
					for (Itinerario i : h.getItinerarios()) {
						// i.setCodigo(0);
						i.setHorario(h);
						getDao(Itinerario.class).createOrUpdate(i);
					}
				}

				// Itinerários
				for (Itinerario i : linha.getItinerarios()) {
					// i.setCodigo(0);
					i.setLinha(linha);
					getDao(Itinerario.class).createOrUpdate(i);
				}

				return null;

			}
		});
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> listarDestinosParaPesquisa() throws SQLException {
		String sql = "SELECT DISTINCT LTRIM(RTRIM(destino)) FROM linha UNION SELECT DISTINCT LTRIM(RTRIM(origem)) FROM linha";
		GenericRawResults<String> result = getDao().queryRaw(sql, new RawRowMapper<String>() {

			@Override
			public String mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				return resultColumns[0];
			}
		});
		return (ArrayList<String>) result.getResults();
	}

	@SuppressWarnings("unchecked")
	public List<Linha> listarFavoritos() throws SQLException {
		return List(newQuery().where().eq("isFavorito", true).prepare());
	}

	@SuppressWarnings("unchecked")
	public List<Linha> listarUltimosVisualizados() throws SQLException {
		long maxResults = 5;
		return List(newQuery().limit(maxResults).orderBy("ultimaVisualizacao", false).where().eq("isFavorito", false).and()
				.isNotNull("ultimaVisualizacao").prepare());
	}

	@SuppressWarnings("unchecked")
	public void favoritar(String codigo) throws SQLException {
		Linha linha = get(codigo);
		linha.setFavorito(!linha.isFavorito());
		getDao().update(linha);
	}

	@SuppressWarnings("unchecked")
	public void registrarUltimaVisita(String codigo, DateTime data) throws SQLException {
		Linha linha = get(codigo);
		linha.setUltimaVisualizacao(data);
		getDao().update(linha);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Linha> buscarLinhas(String de, String para) throws SQLException {

		Where where = newQuery().where();
		if (de != null && !de.trim().isEmpty())
			where = where.like("origem", "%" + de + "%");

		if (para != null && !para.trim().isEmpty())
			if (de != null && !de.trim().isEmpty())
				where.and().like("destino", "%" + para + "%");
			else
				where.like("destino", "%" + para + "%");

		return List(where.prepare());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Linha> listAll() throws SQLException {

		return List(newQuery().orderBy("origem", true).orderBy("destino", true).prepare());
	}

	@SuppressWarnings("unchecked")
	public void limparTodasAsVisualizacoes() throws SQLException {

		UpdateBuilder<Linha, String> updateBuilder = getDao().updateBuilder();
		updateBuilder.updateColumnValue("ultimaVisualizacao", null);
		updateBuilder.where().isNotNull("ultimaVisualizacao");
		updateBuilder.update();
	}

	public void prepararSincronizacao() throws SQLException {
		getDao().executeRaw("UPDATE linha SET dataAtualizacao = null");
		getDao().executeRaw("DELETE FROM horario");
		getDao().executeRaw("DELETE FROM itinerario");
	}

	public void finalizarSincronizacao() throws SQLException {
		getDao().executeRaw("DELETE FROM linha WHERE dataAtualizacao IS NULL;");
		//getDao().executeRaw("DELETE FROM horario WHERE linha_id NOT IN (SELECT codigo FROM linha);");
		//getDao().executeRaw("DELETE FROM itinerario WHERE linha_id NOT NULL AND linha_id NOT IN (SELECT codigo FROM linha);");		
		getDao().executeRaw("DELETE FROM alarme WHERE linha_id NOT IN (SELECT codigo FROM linha);");
	}

}
