package br.srv.rcs.onibus.repository;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Horario;

public class HorarioRepository extends RepositoryBase<Horario> {

	public HorarioRepository(Context context) {
		super(context);
	}

}
