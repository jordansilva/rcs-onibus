package br.srv.rcs.onibus.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Alarme;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;

public class AlarmeRepository extends RepositoryBase<Alarme> {

	public AlarmeRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> listarCodigoLinhas() throws SQLException {
		String sql = "SELECT DISTINCT linha_id FROM alarme";
		GenericRawResults<String> result = getDao().queryRaw(sql, new RawRowMapper<String>() {

			@Override
			public String mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				return resultColumns[0];
			}
		});
		return (ArrayList<String>) result.getResults();
	}

	@SuppressWarnings("unchecked")
	public List<Alarme> buscarAlarmeLinha(String codigoLinha) throws SQLException {

		return newQuery().where().eq("linha_id", codigoLinha).query();

	}
}
