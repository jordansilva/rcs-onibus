package br.srv.rcs.onibus.repository;

import java.sql.SQLException;

import org.joda.time.DateTime;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Propaganda;

import com.j256.ormlite.stmt.DeleteBuilder;

public class PropagandaRepository extends RepositoryBase<Propaganda> {

	public PropagandaRepository(Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.util.List<Propaganda> listAll() throws SQLException {
		return List(newQuery().orderBy("peso", false).where().gt("dataExpiracao", DateTime.now()).prepare());
	}

	@SuppressWarnings("unchecked")
	public void deleteAll() throws SQLException {
		DeleteBuilder<Propaganda, Integer> builder = getDao().deleteBuilder();
		builder.delete();
	}

}
