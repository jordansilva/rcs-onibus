package br.srv.rcs.onibus.repository;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Empresa;

public class EmpresaRepository extends RepositoryBase<Empresa> {

	public EmpresaRepository(Context context) {
		super(context);
	}

}
