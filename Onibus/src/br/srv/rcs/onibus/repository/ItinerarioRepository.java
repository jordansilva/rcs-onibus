package br.srv.rcs.onibus.repository;

import android.content.Context;
import br.srv.rcs.onibus.business.domain.Itinerario;

public class ItinerarioRepository extends RepositoryBase<Itinerario> {

	public ItinerarioRepository(Context context) {
		super(context);
	}

}
