package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import br.srv.rcs.onibus.business.PropagandaService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Propaganda;
import br.srv.rcs.onibus.helper.Constants;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.fragment_propaganda)
public class PropagandaFragment extends BaseFragment implements Callback {

	private ArrayList<Propaganda> propagandas;
	private ArrayList<Propaganda> propagandasExibidas;
	private Propaganda propagandaAtual;
	private Timer timer;

	@ViewById
	ImageView imgPublicidade;

	@Bean
	PropagandaService service;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("propagandas"))
				propagandas = (ArrayList<Propaganda>) savedInstanceState.getSerializable("propagandas");

			if (savedInstanceState.containsKey("propagandasExibidas"))
				propagandasExibidas = (ArrayList<Propaganda>) savedInstanceState.getSerializable("propagandasExibidas");

			if (savedInstanceState.containsKey("propagandas"))
				propagandaAtual = (Propaganda) savedInstanceState.getSerializable("propagandaAtual");
		}
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	protected void load() {
		super.load();
		
		timer = new Timer();
		
		if (propagandasExibidas == null)
		{
			propagandasExibidas = new ArrayList<Propaganda>();
			carregarPublicidade();
		}
		else
			exibirPropagandas();
		
	}

	@Background
	protected void carregarPublicidade() {

		service.listar(new ServiceListener() {

			@Override
			public void onPreInit() {
				// TODO Auto-generated method stub

			}

			@SuppressWarnings("unchecked")
			@Override
			public void onPostExecute(RequestResult result) {
				if (result.isError())
					propagandas = new ArrayList<Propaganda>();
				else
					propagandas = (ArrayList<Propaganda>) result.getData();

				exibirPropagandas();
			}
		});
	}

	@UiThread
	protected void exibirPropagandas() {
		if (propagandas.size() > 0) {
			for (Propaganda p : propagandas) {
				if (!propagandasExibidas.contains(p)) {
					propagandaAtual = p;
					if (p.getBanner() != null && !p.getBanner().isEmpty())
						Picasso.with(getActivity()).load(p.getBanner()).error(R.drawable.publicidade2).into(imgPublicidade, this);
					else {
						imgPublicidade.setImageResource(R.drawable.publicidade2);
						tickTimer();
					}

					break;
				}
			}

			if (propagandaAtual == null) {
				propagandasExibidas.clear();
				exibirPropagandas();
			}
		} else
			imgPublicidade.setImageResource(R.drawable.publicidade2);
	}

	@Click(R.id.imgPublicidade)
	protected void clickPublicidade(View v) {
		String url = Constants.URL_RCS;
		if (propagandaAtual != null && propagandaAtual.getUrl() != null && !propagandaAtual.getUrl().isEmpty())
			url = propagandaAtual.getUrl();

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	@Override
	public void onError() {
		tickTimer();
	}

	@Override
	public void onSuccess() {
		tickTimer();
	}

	private void tickTimer() {
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				propagandasExibidas.add(propagandaAtual);
				propagandaAtual = null;
				exibirPropagandas();
			}
		}, propagandaAtual.getPeso() * 10000);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (propagandas != null && propagandasExibidas != null && propagandaAtual != null) {
			outState.putSerializable("propagandas", propagandas);
			outState.putSerializable("propagandasExibidas", propagandasExibidas);
			outState.putSerializable("propagandaAtual", propagandaAtual);
		}

		super.onSaveInstanceState(outState);
	}
}
