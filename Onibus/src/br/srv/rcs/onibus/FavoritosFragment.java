package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.ui.DialogLinha;
import br.srv.rcs.onibus.ui.DialogLinha.DialogLinhaListener;
import br.srv.rcs.onibus.ui.Item;
import br.srv.rcs.onibus.ui.adapter.AdapterLinha;

import com.kanak.emptylayout.EmptyLayout;

@EFragment(R.layout.fragment_favoritos)
public class FavoritosFragment extends BaseFragment implements OnItemClickListener, OnItemLongClickListener {

	private EmptyLayout mEmptyMinhasLinhas;

	@ViewById
	protected ListView listView;

	@Bean
	protected OnibusService onibusService;

	@Override
	protected void load() {
		super.load();

		mEmptyMinhasLinhas = new EmptyLayout(getActivity(), listView);
		carregarMinhasLinhas();
	}

	@Override
	protected void configureActionBar() {
		// TODO Auto-generated method stub
		super.configureActionBar();
		getActivity().setTitle(R.string.menu_favoritos);
	}

	@Background
	protected void carregarMinhasLinhas() {

		onibusService.buscarMinhasLinhas(new ServiceListener() {

			@Override
			public void onPreInit() {
				showLoading(mEmptyMinhasLinhas);
			}

			@Override
			public void onPostExecute(final RequestResult result) {

				if (!result.isError()) {
					@SuppressWarnings("unchecked")
					List<Linha> linhas = (List<Linha>) result.getData();
					carregarLinhas(linhas);

					if (linhas.isEmpty())
						showEmpty(mEmptyMinhasLinhas);
				} else
					showError(mEmptyMinhasLinhas);
			}
		});
	}

	@UiThread
	protected void carregarLinhas(List<Linha> linhas) {

		listView.setAdapter(new AdapterLinha(getActivity(), linhas, R.layout.fragment_item_linha_list));
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);
		DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1)
				carregarMinhasLinhas();
			if (requestCode == 2) {
				carregarMinhasLinhas();
				Toast.makeText(getActivity(), R.string.alarme_criado, Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

		final Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);

		List<Item> items = new ArrayList<Item>();
		items.add(new Item(1, getString(R.string.dialog_visualizar_detalhe)));
		items.add(new Item(2, getString(R.string.dialog_agendar_add)));
		if (linhaOnibus.isFavorito())
			items.add(new Item(3, getString(R.string.dialog_favoritos_del)));
		else
			items.add(new Item(3, getString(R.string.dialog_favoritos_add)));

		DialogLinha dialog = new DialogLinha(linhaOnibus.getNome(), items, new DialogLinhaListener() {

			@Override
			public void onItemClick(Item item) {
				if (item.getId() == 1)
					DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

				if (item.getId() == 2)
					AlarmeActivity_.intent(getActivity()).linhaOnibus(linhaOnibus).startForResult(2);

				if (item.getId() == 3)
					favoritarLinha(linhaOnibus);

			}
		});
		dialog.show(getFragmentManager(), DialogLinha.class.getSimpleName());

		return true;
	}

	@Background
	protected void favoritarLinha(final Linha linha) {
		onibusService.favoritarLinha(linha.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					int texto = linha.isFavorito() ? R.string.linha_favorito_removido : R.string.linha_favorito_adicionada;
					showToast(texto);
					carregarMinhasLinhas();
				}
			}
		});
	}

}
