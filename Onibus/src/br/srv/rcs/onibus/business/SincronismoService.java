package br.srv.rcs.onibus.business;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.business.domain.Propaganda;
import br.srv.rcs.onibus.repository.LinhaRepository;
import br.srv.rcs.onibus.repository.PropagandaRepository;
import br.srv.rcs.onibus.service.CustomErrorHandler;
import br.srv.rcs.onibus.service.SincronizarService;

@EBean
public class SincronismoService extends ServiceBase {

	@RestService
	SincronizarService sincronizarService;

	@Bean
	CustomErrorHandler customErrorHandler;

	@AfterInject
	protected void init() {
		sincronizarService.setRestErrorHandler(customErrorHandler);

		ClientHttpRequestFactory requestFactory = sincronizarService.getRestTemplate().getRequestFactory();
		if (requestFactory instanceof SimpleClientHttpRequestFactory) {
			((SimpleClientHttpRequestFactory) requestFactory).setConnectTimeout(5000);
			// ((SimpleClientHttpRequestFactory)
			// requestFactory).setReadTimeout(30000);
		} else if (requestFactory instanceof HttpComponentsClientHttpRequestFactory) {
			((HttpComponentsClientHttpRequestFactory) requestFactory).setConnectTimeout(5000);
			// ((HttpComponentsClientHttpRequestFactory)
			// requestFactory).setReadTimeout(30000);
		}

	}

	public synchronized void sincronizar(DateTime date, ServiceListener listener) {

		synchronized (this) {

			if (listener != null) {
				listener.onPreInit();
				RequestResult result = new RequestResult();
				try {
					// Sincronizar Linhas
					List<Linha> linhas = (List<Linha>) sincronizarService.sincronizar(date.toString("ddMMyyyy"));

					if (linhas.size() > 0) {

						LinhaRepository linhaRepository = new LinhaRepository(context);
						linhaRepository.prepararSincronizacao();

						for (Linha linha : linhas) {
							linhaRepository.save(linha);
						}

						linhaRepository.finalizarSincronizacao();
					}

					// Sincronizar Publicidade
					List<Propaganda> propagandas = (List<Propaganda>) sincronizarService.publicidade();
					PropagandaRepository propagandaRepository = new PropagandaRepository(context);
					propagandaRepository.deleteAll();

					for (Propaganda propaganda : propagandas) {
						propagandaRepository.save(propaganda);
					}

					result.setData(linhas);
				} catch (SQLException e) {
					result.setError(true);

				} catch (Exception e) {
					result.setError(true);
				}

				if (listener != null)
					listener.onPostExecute(result);
			}
		}
	}
}
