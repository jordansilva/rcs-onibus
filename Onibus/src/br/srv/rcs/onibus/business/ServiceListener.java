package br.srv.rcs.onibus.business;

public interface ServiceListener {
	abstract void onPreInit();

	abstract void onPostExecute(RequestResult result);
}
