package br.srv.rcs.onibus.business;

import java.sql.SQLException;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;

import android.app.AlarmManager;
import br.srv.rcs.onibus.repository.PropagandaRepository;

@EBean
public class PropagandaService extends ServiceBase {

	@SystemService
	AlarmManager alarmManager;

	public void listar(ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				PropagandaRepository repository = new PropagandaRepository(context);
				result.setData(repository.listAll());
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

}
