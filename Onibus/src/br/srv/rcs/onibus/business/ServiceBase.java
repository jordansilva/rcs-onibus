package br.srv.rcs.onibus.business;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.content.Context;

@EBean
public abstract class ServiceBase {

	@RootContext
	protected Context context;

}
