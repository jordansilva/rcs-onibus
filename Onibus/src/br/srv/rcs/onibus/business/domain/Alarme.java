package br.srv.rcs.onibus.business.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.text.TextUtils;
import br.srv.rcs.onibus.helper.StringHelper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "alarme")
public class Alarme extends DomainBase {

	@Expose
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	@SerializedName("Codigo")
	private int codigo;

	@Expose
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	@SerializedName("Linha")
	private Linha linha;

	@Expose
	@DatabaseField
	private DateTime data;

	@Expose
	@DatabaseField
	private DateTime horario;

	@Expose
	@DatabaseField
	private boolean repetirSemanalmente;

	@Expose
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private Integer[] repetirDias;

	@Expose
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private UUID[] alarmesId;

	@DatabaseField
	private int color;

	public Alarme() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public LocalDate getData() {
		return data.toLocalDate();
	}

	public void setData(LocalDate data) {
		this.data = data.toDateTimeAtStartOfDay();
	}

	public LocalTime getHorario() {
		return horario.toLocalTime();
	}

	public void setHorario(LocalTime horario) {
		this.horario = horario.toDateTimeToday();
	}

	public boolean isRepetirSemanalmente() {
		return repetirSemanalmente;
	}

	public void setRepetirSemanalmente(boolean repetirSemanalmente) {
		this.repetirSemanalmente = repetirSemanalmente;
	}

	public List<Integer> getRepetirDias() {
		if (repetirDias == null)
			return new ArrayList<Integer>();

		return Arrays.asList(repetirDias);
	}

	public String getDiasDoAlarme() {
		List<Integer> dias = (List<Integer>) getRepetirDias();

		Collections.sort(dias, new Comparator<Integer>() {

			@Override
			public int compare(Integer lhs, Integer rhs) {
				return lhs.compareTo(rhs);
			}
		});

		String[] diasSemana = new String[dias.size()];

		for (int i = 0; i < dias.size(); i++) {
			diasSemana[i] = StringHelper.formatDayOfWeek(dias.get(i));
		}

		return TextUtils.join(", ", diasSemana);
	}

	public void adicionarDia(int dia) {
		if (repetirDias == null)
			repetirDias = new Integer[] { dia };
		else {
			ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(repetirDias));
			if (!list.contains(dia))
				list.add(dia);

			repetirDias = list.toArray(new Integer[] {});
		}
	}

	public void removerDia(int dia) {
		if (repetirDias != null) {
			ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(repetirDias));
			if (list.contains(dia))
				list.remove(list.indexOf(dia));

			repetirDias = list.toArray(new Integer[] {});
		}
	}

	public void setRepetirDias(Collection<Integer> repetirDias) {
		this.repetirDias = repetirDias.toArray(this.repetirDias);
	}

	public int getCor() {
		return color;
	}

	public void setCor(int cor) {
		this.color = cor;
	}

	public UUID[] getAlarmesId() {
		return alarmesId;
	}

	public void setAlarmesId(UUID[] alarmesId) {
		this.alarmesId = alarmesId;
	}

	public boolean isAgendadoHoje() {

		LocalDate hoje = LocalDate.now();

		if (data.toLocalDate().isEqual(hoje))
			return true;

		if (repetirSemanalmente && repetirDias != null && repetirDias.length > 0) {
			List<Integer> dias = Arrays.asList(repetirDias);
			if (dias.contains(hoje.getDayOfWeek()))
				return true;
		}

		return false;
	}

}
