package br.srv.rcs.onibus.business.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "empresa")
public class Empresa extends DomainBase {

	@Expose
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	@SerializedName("Codigo")
	private int codigo;

	@Expose
	@DatabaseField
	@SerializedName("Nome")
	private String nome;

	@Expose
	@DatabaseField
	@SerializedName("Email")
	private String email;

	@Expose
	@DatabaseField
	@SerializedName("Telefone")
	private String telefone;

	@Expose
	@DatabaseField
	@SerializedName("Url")
	private String logo;

	@Expose
	@ForeignCollectionField(eager = false)
	private ForeignCollection<Linha> linhas;

	public Empresa() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public ForeignCollection<Linha> getLinhas() {
		return linhas;
	}

	public void setLinhas(ForeignCollection<Linha> linhas) {
		this.linhas = linhas;
	}

}
