package br.srv.rcs.onibus.business.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import br.srv.rcs.onibus.helper.Constants.DiaOnibus;
import br.srv.rcs.onibus.helper.HorarioHelper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "linha")
public class Linha extends DomainBase {

	@DatabaseField(id = true, useGetSet = true)
	private String codigo;

	@Expose
	@DatabaseField(uniqueCombo = true)
	@SerializedName("Codigo")
	private int codigoLinha;

	@Expose
	@DatabaseField
	@SerializedName("Nome")
	private String nome;

	@Expose
	@DatabaseField
	@SerializedName("Origem")
	private String origem;

	@Expose
	@DatabaseField
	@SerializedName("Destino")
	private String destino;

	@Expose
	@DatabaseField(uniqueCombo = true)
	@SerializedName("Sentido")
	private int sentido;

	@Expose
	@DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
	@SerializedName("Empresa")
	private Empresa empresa;

	@ForeignCollectionField(columnName = "horarios", eager = true)
	private ForeignCollection<Horario> foreignHorarios;

	@Expose
	@SerializedName("Horarios")
	private Collection<Horario> horarios;

	@ForeignCollectionField(columnName = "itinerarios", eager = true)
	private ForeignCollection<Itinerario> foreignItinerarios;

	@Expose
	@SerializedName("ItinerarioPadrao")
	private Collection<Itinerario> itinerarios;

	@Expose
	@DatabaseField
	@SerializedName("DataAtualizacao")
	private DateTime dataAtualizacao;

	@Expose
	@DatabaseField
	@SerializedName("DataVigencia")
	private DateTime dateVigencia;

	@DatabaseField
	private boolean isFavorito;

	@DatabaseField
	private DateTime ultimaVisualizacao;

	private boolean isExisteAlarme;

	private List<Alarme> alarmes;
	private List<Horario> horariosDiaSemana;
	private List<Horario> horariosSabado;
	private List<Horario> horariosDomingo;
	private Horario proximoHorario;

	public Linha() {
		super();

		horariosDiaSemana = new ArrayList<Horario>();
		horariosSabado = new ArrayList<Horario>();
		horariosDomingo = new ArrayList<Horario>();
	}

	public String getCodigo() {
		return String.format(Locale.getDefault(), "%d-%d", getCodigoExterno(), getSentido());
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getCodigoExterno() {
		return this.codigoLinha;
	}

	public void setCodigoExterno(int codigo) {
		this.codigoLinha = codigo;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public DateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(DateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public DateTime getDateVigencia() {
		return dateVigencia;
	}

	public void setDateVigencia(DateTime dateVigencia) {
		this.dateVigencia = dateVigencia;
	}

	public Collection<Horario> getHorarios() {

		if (horarios == null)
			if (foreignHorarios != null) {
				horarios = new ArrayList<Horario>(foreignHorarios);
				Collections.sort((List<Horario>) horarios, new Comparator<Horario>() {

					@Override
					public int compare(Horario lhs, Horario rhs) {
						LocalTime lhsTime = lhs.getHorarioSaida().toLocalTime();
						LocalTime rhsTime = rhs.getHorarioSaida().toLocalTime();

						return lhsTime.compareTo(rhsTime);
					}
				});
			} else
				horarios = new ArrayList<Horario>();

		return horarios;
	}

	public void setHorarios(Collection<Horario> horarios) {
		this.horarios = horarios;
	}

	public Collection<Itinerario> getItinerarios() {

		if (itinerarios == null)
			if (foreignItinerarios != null)
				itinerarios = new ArrayList<Itinerario>(foreignItinerarios);
			else
				itinerarios = new ArrayList<Itinerario>();

		return itinerarios;
	}

	public void setItinerarios(Collection<Itinerario> itinerarios) {
		this.itinerarios = itinerarios;
	}

	public ForeignCollection<Horario> getForeignHorarios() {
		return foreignHorarios;
	}

	public void setForeignHorarios(ForeignCollection<Horario> foreignHorarios) {
		this.foreignHorarios = foreignHorarios;
	}

	public ForeignCollection<Itinerario> getForeignItinerarios() {
		return foreignItinerarios;
	}

	public void setForeignItinerarios(ForeignCollection<Itinerario> foreignItinerarios) {
		this.foreignItinerarios = foreignItinerarios;
	}

	public boolean isFavorito() {
		return isFavorito;
	}

	public void setFavorito(boolean isFavorito) {
		this.isFavorito = isFavorito;
	}

	public String getNome() {
		return origem + "/" + destino;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSentido() {
		return sentido;
	}

	public void setSentido(int sentido) {
		this.sentido = sentido;
	}

	public Horario getProximoHorario() {
		if (proximoHorario == null)
			proximoHorario = HorarioHelper.buscarProximoHorario(getHorarios());
		
		return proximoHorario;
	}
	
	public void setProximoHorario(Horario proximoHorario)
	{
		this.proximoHorario = proximoHorario;
	}

	public DateTime getUltimaVisualizacao() {
		return ultimaVisualizacao;
	}

	public void setUltimaVisualizacao(DateTime ultimaVisita) {
		this.ultimaVisualizacao = ultimaVisita;
	}

	public boolean isExisteAlarme() {
		return isExisteAlarme;
	}

	public void setExisteAlarme(boolean isExisteAlarme) {
		this.isExisteAlarme = isExisteAlarme;
	}

	public void setAlarmes(List<Alarme> alarmes) {
		this.alarmes = alarmes;
	}

	public List<Alarme> getAlarmes() {
		return alarmes;
	}

	public List<Alarme> getAlarmesDoDia() {

		List<Alarme> listaAlarmes = new ArrayList<Alarme>();
		if (alarmes != null && alarmes.size() > 0) {
			for (Alarme a : alarmes) {
				if (a.isAgendadoHoje())
					listaAlarmes.add(a);
			}

		}
		return listaAlarmes;
	}

	public String getCodigoLinhaInverso() {
		String[] codigoInverso = codigo.split("-");
		if (codigoInverso[1].equalsIgnoreCase("1"))
			codigoInverso[1] = "2";
		else
			codigoInverso[1] = "1";

		return codigoInverso[0] + "-" + codigoInverso[1];
	}

	@Override
	public String toString() {
		return getOrigem() + "/" + getDestino();
	}

	public Horario getHorario(int codigo) {
		for (Horario h : getHorarios()) {
			if (h.getCodigo() == codigo)
				return h;
		}
		return null;
	}

	public Collection<Horario> getHorarios(DiaOnibus dia) {
		if (getHorarios() != null && horariosDiaSemana.size() == 0 && horariosSabado.size() == 0 && horariosDomingo.size() == 0) {

			for (Horario h : horarios) {
				h.setLinha(this);
				if (h.isDiaSemana())
					horariosDiaSemana.add(h);
				if (h.isSabado())
					horariosSabado.add(h);

				if (h.isDomingo())
					horariosDomingo.add(h);
			}
		}

		if (dia == DiaOnibus.SEMANA)
			return horariosDiaSemana;
		else if (dia == DiaOnibus.SABADO)
			return horariosSabado;
		else
			return horariosDomingo;
	}

}
