package br.srv.rcs.onibus.business.domain;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "horario")
public class Horario extends DomainBase {

	// @Expose
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	// @SerializedName("Codigo")
	private int codigo;

	@Expose
	@DatabaseField
	@SerializedName("Codigo")
	private long codigoHorario;

	@DatabaseField(foreign = true)
	private Linha linha;

	@ForeignCollectionField(eager = true, columnName = "itinerarios")
	private ForeignCollection<Itinerario> foreignItinerarios;

	@Expose
	@SerializedName("Itinerarios")
	private Collection<Itinerario> itinerarios;

	@Expose
	@DatabaseField
	@SerializedName("HoraSaida")
	private DateTime horarioSaida;

	@Expose
	@DatabaseField
	@SerializedName("HoraChegada")
	private DateTime horarioChegada;

	@Expose
	@DatabaseField
	@SerializedName("DiaSemana")
	private boolean diaSemana;

	@Expose
	@DatabaseField
	@SerializedName("Sabado")
	private boolean sabado;

	@Expose
	@DatabaseField
	@SerializedName("Domingo")
	private boolean domingo;

	@Expose
	@DatabaseField
	@SerializedName("Valor")
	private double valor;

	@Expose
	@DatabaseField
	@SerializedName("Descricao")
	private String descricao;

	public Horario() {
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public Collection<Itinerario> getItinerarios() {

		if (itinerarios == null)
			if (foreignItinerarios != null)
				itinerarios = new ArrayList<Itinerario>(foreignItinerarios);
			else
				itinerarios = new ArrayList<Itinerario>();

		return itinerarios;
	}

	public void setItinerarios(ForeignCollection<Itinerario> itinerarios) {
		this.itinerarios = itinerarios;
	}

	public DateTime getHorarioSaida() {
		return horarioSaida;
	}

	public void setHorarioSaida(DateTime horarioSaida) {
		this.horarioSaida = horarioSaida;
	}

	public DateTime getHorarioChegada() {
		return horarioChegada;
	}

	public void setHorarioChegada(DateTime horarioChegada) {
		this.horarioChegada = horarioChegada;
	}

	public boolean isDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(boolean diaSemana) {
		this.diaSemana = diaSemana;
	}

	public boolean isSabado() {
		return sabado;
	}

	public void setSabado(boolean sabado) {
		this.sabado = sabado;
	}

	public boolean isDomingo() {
		return domingo;
	}

	public void setDomingo(boolean domingo) {
		this.domingo = domingo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean funcionaHoje() {
		if (isDiaSemana() && DateTime.now().getDayOfWeek() <= 5)
			return true;
		else if (isSabado() && DateTime.now().getDayOfWeek() == 6)
			return true;
		else if (isDomingo() && DateTime.now().getDayOfWeek() == 7)
			return true;
		else
			return false;
	}

	public int getTempoDeEspera() {
		return Minutes.minutesBetween(LocalTime.now(), horarioSaida.toLocalTime()).getMinutes();
	}

}
