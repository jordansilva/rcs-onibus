package br.srv.rcs.onibus.business.domain;

import org.joda.time.DateTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "propaganda")
public class Propaganda extends DomainBase {

	@Expose
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	@SerializedName("Codigo")
	private int codigo;

	@Expose
	@DatabaseField
	@SerializedName("Url")
	private String url;

	@Expose
	@DatabaseField
	@SerializedName("Banner")
	private String banner;

	@Expose
	@DatabaseField
	@SerializedName("DataExpiracao")
	private DateTime dataExpiracao;

	@Expose
	@DatabaseField
	@SerializedName("Peso")
	private int peso;

	public Propaganda() {
		super();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public DateTime getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(DateTime dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

}
