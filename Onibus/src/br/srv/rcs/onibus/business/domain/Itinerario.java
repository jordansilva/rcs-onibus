package br.srv.rcs.onibus.business.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@SuppressWarnings("serial")
@DatabaseTable(tableName = "itinerario")
public class Itinerario extends DomainBase {

	@Expose
	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	@SerializedName("Codigo")
	private int codigo;

	@DatabaseField(foreign = true)
	private Linha linha;

	@Expose
	@DatabaseField
	@SerializedName("Ordem")
	private int ordem;

	@Expose
	@DatabaseField
	@SerializedName("Latitude")
	private double latitude;

	@Expose
	@DatabaseField
	@SerializedName("Longitude")
	private double longitude;

	@Expose
	@DatabaseField
	@SerializedName("Descricao")
	private String descricao;

	@DatabaseField(foreign = true)
	private Horario horario;

	public Itinerario() {
		// TODO Auto-generated constructor stub
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

}
