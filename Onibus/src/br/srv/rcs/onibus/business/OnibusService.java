package br.srv.rcs.onibus.business;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.rest.RestService;
import org.joda.time.DateTime;

import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.repository.AlarmeRepository;
import br.srv.rcs.onibus.repository.LinhaRepository;
import br.srv.rcs.onibus.service.CustomErrorHandler;
import br.srv.rcs.onibus.service.SincronizarService;

@EBean
public class OnibusService extends ServiceBase {

	@RestService
	SincronizarService sincronizarService;

	@Bean
	CustomErrorHandler customErrorHandler;

	@AfterInject
	protected void init() {
		sincronizarService.setRestErrorHandler(customErrorHandler);
	}

	public void listarDestinosParaPesquisa(ServiceListener listener) {
		if (listener != null) {
			RequestResult result = new RequestResult();
			try {
				LinhaRepository repository = new LinhaRepository(context);
				result.setData(repository.listarDestinosParaPesquisa());
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void listarTodasAsLinhas(ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {

				AlarmeRepository alarmeRepository = new AlarmeRepository(context);
				List<String> alarmes = alarmeRepository.listarCodigoLinhas();

				LinhaRepository repository = new LinhaRepository(context);
				List<Linha> linhas = repository.listAll();

				for (int i = 0; i < linhas.size(); i++) {
					if (alarmes.contains(linhas.get(i).getCodigo()))
						linhas.get(i).setExisteAlarme(true);
				}

				result.setData(linhas);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void buscarLinhas(String de, String para, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				AlarmeRepository alarmeRepository = new AlarmeRepository(context);
				List<String> alarmes = alarmeRepository.listarCodigoLinhas();

				LinhaRepository repository = new LinhaRepository(context);
				List<Linha> linhas = repository.buscarLinhas(de, para);

				for (int i = 0; i < linhas.size(); i++) {
					if (alarmes.contains(linhas.get(i).getCodigo()))
						linhas.get(i).setExisteAlarme(true);
				}

				result.setData(linhas);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void buscarMinhasLinhas(ServiceListener listener) {
		if (listener != null) {

			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				AlarmeRepository alarmeRepository = new AlarmeRepository(context);
				List<String> alarmes = alarmeRepository.listarCodigoLinhas();

				LinhaRepository repository = new LinhaRepository(context);
				List<Linha> linhas = repository.listarFavoritos();

				for (int i = 0; i < linhas.size(); i++) {
					if (alarmes.contains(linhas.get(i).getCodigo()))
						linhas.get(i).setExisteAlarme(true);
				}

				result.setData(linhas);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void buscarUltimasLinhasVisualizadas(ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				AlarmeRepository alarmeRepository = new AlarmeRepository(context);
				List<String> alarmes = alarmeRepository.listarCodigoLinhas();

				LinhaRepository repository = new LinhaRepository(context);
				List<Linha> linhas = repository.listarUltimosVisualizados();

				for (int i = 0; i < linhas.size(); i++) {
					if (alarmes.contains(linhas.get(i).getCodigo()))
						linhas.get(i).setExisteAlarme(true);
				}

				result.setData(linhas);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void favoritarLinha(String codigo, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();

			LinhaRepository repository = new LinhaRepository(context);
			try {
				repository.favoritar(codigo);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void registrarUltimaVisualizacao(String codigo, DateTime data, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();

			LinhaRepository repository = new LinhaRepository(context);
			try {
				repository.registrarUltimaVisita(codigo, data);
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void limparTodasAsBuscas(ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();

			LinhaRepository repository = new LinhaRepository(context);
			try {
				repository.limparTodasAsVisualizacoes();
			} catch (SQLException e) {
				result.setError(true);
				result.setData(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void buscarLinha(String codigo, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				AlarmeRepository alarmeRepository = new AlarmeRepository(context);
				List<Alarme> alarmes = alarmeRepository.buscarAlarmeLinha(codigo);

				LinhaRepository repository = new LinhaRepository(context);
				Linha linha = repository.get(codigo);

				if (linha == null)
					throw new SQLException("Not found"); 
					
				if (alarmes != null && alarmes.size() > 0) {
					linha.setExisteAlarme(true);
					linha.setAlarmes(alarmes);
				}

				
				result.setData(linha);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}
}
