package br.srv.rcs.onibus.business;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.repository.AlarmeRepository;
import br.srv.rcs.onibus.ui.receiver.AlarmReceiver_;

@EBean
public class AlarmeService extends ServiceBase {

	@SystemService
	AlarmManager alarmManager;

	public void listarMeusAlarmes(ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();

			RequestResult result = new RequestResult();
			try {
				AlarmeRepository repository = new AlarmeRepository(context);
				result.setData(repository.listAll());
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void salvarAlarme(Alarme alarme, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();

			try {

				AlarmeRepository repository = new AlarmeRepository(context);
				repository.save(alarme);

				Calendar calendar = Calendar.getInstance(Locale.getDefault());
				calendar.setTime(alarme.getData().toDate());
				calendar.set(Calendar.HOUR_OF_DAY, alarme.getHorario().getHourOfDay());
				calendar.set(Calendar.MINUTE, alarme.getHorario().getMinuteOfHour());

				if (alarme.isRepetirSemanalmente() && !alarme.getRepetirDias().isEmpty())
					alarme.setAlarmesId(createSystemAlarmRepeat(alarme, calendar).toArray(new UUID[] {}));
				else {
					UUID uuid = UUID.randomUUID();
					alarme.setAlarmesId(new UUID[] { createSystemAlarm(uuid, alarme, calendar) });
				}

				repository.save(alarme);

			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public void removerAlarme(int alarmeId, ServiceListener listener) {
		if (listener != null) {
			listener.onPreInit();
			RequestResult result = new RequestResult();
			try {
				AlarmeRepository repository = new AlarmeRepository(context);
				Alarme alarme = repository.get(alarmeId);

				removeSystemAlarm(alarme);

				repository.deleteById(alarmeId);
			} catch (SQLException e) {
				result.setError(true);
				result.setMessage(e.getMessage());
			}

			listener.onPostExecute(result);
		}
	}

	public List<UUID> createSystemAlarmRepeat(Alarme alarme, Calendar calendar) {
		List<UUID> codigosAlarmes = new ArrayList<UUID>();
		for (int dia : alarme.getRepetirDias()) {
			Calendar calendarRepeat = Calendar.getInstance(Locale.getDefault());
			calendarRepeat.setTime(calendar.getTime());
			calendarRepeat.set(Calendar.DAY_OF_WEEK, dia);
			if (calendarRepeat.getTimeInMillis() < Calendar.getInstance(Locale.getDefault()).getTimeInMillis())
				calendarRepeat.add(Calendar.DAY_OF_YEAR, 7);

			UUID uuid = UUID.randomUUID();
			codigosAlarmes.add(createSystemAlarm(uuid, alarme, calendarRepeat));
		}

		return codigosAlarmes;
	}

	@SuppressLint("NewApi")
	public UUID createSystemAlarm(UUID uuid, Alarme alarme, Calendar date) {

		Intent intent = new Intent(context, AlarmReceiver_.class);
		intent.putExtra("ARGS", alarme);
		intent.putExtra("REPETIR", alarme.isRepetirSemanalmente());
		intent.setAction("AlarmReceiver" + uuid.toString());

		PendingIntent alarmIntent = PendingIntent.getBroadcast(context, alarme.getCodigo(), intent, 0);

		SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm:ss a", Locale.getDefault());

		Log.w("RCS:AlarmScheduled", String.valueOf(date.getTimeInMillis()) + "-" + format.format(date.getTime()));
		if (alarme.isRepetirSemanalmente())
			if (Build.VERSION.SDK_INT >= 19)
				alarmManager.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), alarmIntent);
			else
				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, alarmIntent);
		else {
			if (Build.VERSION.SDK_INT >= 19)
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), alarmIntent);
			else
				alarmManager.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), alarmIntent);
		}

		return uuid;
	}

	private void removeSystemAlarm(Alarme alarme) {

		for (UUID uuid : alarme.getAlarmesId()) {
			Intent intent = new Intent(context, AlarmReceiver_.class);
			intent.putExtra("ARGS", alarme);
			intent.putExtra("REPETIR", alarme.isRepetirSemanalmente());
			intent.setAction("AlarmReceiver" + uuid.toString());

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarme.getCodigo(), intent, 0);
			PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, alarme.getCodigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
			PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, alarme.getCodigo(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
			if (pendingIntent != null) {
				Log.w("RCS:AlarmeRemovido", uuid.toString());
				alarmManager.cancel(pendingIntent);
			}

			if (pendingIntent2 != null)
				alarmManager.cancel(pendingIntent2);

			if (pendingIntent3 != null)
				alarmManager.cancel(pendingIntent3);
		}
	}

	public void recreateAlarms() {
		try {
			AlarmeRepository repository = new AlarmeRepository(context);
			List<Alarme> alarmes = repository.listAll();

			for (Alarme alarme : alarmes) {

				Calendar calendar = Calendar.getInstance(Locale.getDefault());
				calendar.setTime(alarme.getData().toDate());
				calendar.set(Calendar.HOUR_OF_DAY, alarme.getHorario().getHourOfDay());
				calendar.set(Calendar.MINUTE, alarme.getHorario().getMinuteOfHour());

				if (alarme.isRepetirSemanalmente() && !alarme.getRepetirDias().isEmpty()) {
					for (int i = 0; i < alarme.getRepetirDias().size(); i++) {
						Calendar calendarRepeat = Calendar.getInstance(Locale.getDefault());
						calendarRepeat.setTime(calendar.getTime());
						calendarRepeat.set(Calendar.DAY_OF_WEEK, alarme.getRepetirDias().get(i));
						if (calendarRepeat.getTimeInMillis() < Calendar.getInstance(Locale.getDefault()).getTimeInMillis())
							calendarRepeat.add(Calendar.DAY_OF_YEAR, 7);

						createSystemAlarm(alarme.getAlarmesId()[i], alarme, calendarRepeat);
					}
				} else
					createSystemAlarm(alarme.getAlarmesId()[0], alarme, calendar);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
