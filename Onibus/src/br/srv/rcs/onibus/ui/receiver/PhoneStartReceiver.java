package br.srv.rcs.onibus.ui.receiver;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import br.srv.rcs.onibus.business.AlarmeService;

@EReceiver
public class PhoneStartReceiver extends BroadcastReceiver {

	@Bean
	AlarmeService service;

	@Override
	public void onReceive(Context context, Intent intent) {
		service.recreateAlarms();
	}

}