package br.srv.rcs.onibus.ui.receiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import br.srv.rcs.onibus.business.AlarmeService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.ui.AlarmeNotification;

@EReceiver
public class AlarmReceiver extends BroadcastReceiver {

	@Bean
	AlarmeService service;

	@Override
	public void onReceive(Context context, Intent intent) {

		Log.w("AlarmReceiver:onReceive", DateTime.now().toString("dd/MM/yyyy HH:mm:ss"));
		if (Build.VERSION.SDK_INT >= 19) {
			if (intent.getExtras() != null && intent.getExtras().containsKey("REPETIR")) {
				boolean repetirAlarme = intent.getExtras().getBoolean("REPETIR");
				Alarme alarme = getAlarme(intent);
				if (repetirAlarme)
					reagendarAlarme(context, alarme, intent);
			}

		}

		notificarAlarme(context, intent);
	}

	private void notificarAlarme(Context context, Intent intent) {
		Alarme alarme = getAlarme(intent);
		if (alarme != null) {

			NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = AlarmeNotification.criar(context, alarme);
			notifManager.notify(alarme.getCodigo(), notification);

			if (!alarme.isRepetirSemanalmente())
				service.removerAlarme(alarme.getCodigo(), new ServiceListener() {

					@Override
					public void onPreInit() {
						// TODO Auto-generated method stub

					}

					@Override
					public void onPostExecute(RequestResult result) {
						// TODO Auto-generated method stub

					}
				});
		}
	}

	private Alarme getAlarme(Intent intent) {
		Bundle bundle = intent.getExtras();
		if (bundle.containsKey("ARGS")) {
			Object object = bundle.get("ARGS");
			if (object.getClass().equals(Alarme.class)) {
				Alarme alarme = (Alarme) object;
				return alarme;
			}
		}

		return null;
	}

	@SuppressLint("NewApi")
	private void reagendarAlarme(Context context, Alarme alarme, Intent intent) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

		LocalTime hora = alarme.getHorario();

		Calendar date = Calendar.getInstance(Locale.getDefault());
		date.set(Calendar.HOUR_OF_DAY, hora.getHourOfDay());
		date.set(Calendar.MINUTE, hora.getMinuteOfHour());
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		date.add(Calendar.DAY_OF_YEAR, 7);

		SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm:ss a", Locale.getDefault());
		Log.w("Reschedule Alarm", String.valueOf(date.getTimeInMillis()) + "-" + format.format(date.getTime()));

		PendingIntent alarmIntent = PendingIntent.getBroadcast(context, alarme.getCodigo(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), alarmIntent);
	}
}
