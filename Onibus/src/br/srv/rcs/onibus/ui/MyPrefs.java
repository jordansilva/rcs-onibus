package br.srv.rcs.onibus.ui;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface MyPrefs {

	long ultimaSincronizacao();

}
