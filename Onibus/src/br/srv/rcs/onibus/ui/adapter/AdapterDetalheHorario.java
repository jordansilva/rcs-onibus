package br.srv.rcs.onibus.ui.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apmem.tools.layouts.FlowLayout;
import org.joda.time.LocalTime;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.srv.rcs.onibus.R;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.helper.HorarioHelper;
import br.srv.rcs.onibus.ui.ExtedendTextView;

@SuppressLint("NewApi")
public class AdapterDetalheHorario extends BaseAdapter {

	private Context mContext;
	private SparseArray<List<Horario>> mItems;
	private AdapterDetalheHorarioListener mListener;
	private ArrayList<String> mSublinhas;
	private Horario proximoHorario;

	public AdapterDetalheHorario(Context context, SparseArray<List<Horario>> groups, AdapterDetalheHorarioListener listener) {
		this.mContext = context;
		this.mItems = groups;
		this.mListener = listener;
		this.mSublinhas = new ArrayList<String>();
		carregarDados();
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private void carregarDados() {
		ArrayList<Horario> items = new ArrayList<Horario>();

		for (int i = 0; i < mItems.size(); i++) {
			items.addAll(mItems.valueAt(i));
		}

		this.proximoHorario = HorarioHelper.buscarProximoHorario(items);
		this.mSublinhas = (ArrayList<String>) HorarioHelper.buscarLegendas(items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.fragment_item_horario, parent, false);

			viewHolder = new ViewHolder();
			viewHolder.text = (TextView) convertView.findViewById(R.id.text);
			viewHolder.grid = (FlowLayout) convertView.findViewById(R.id.grid);
			viewHolder.grid.setClickable(false);

			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		int horario = mItems.keyAt(position);
		List<Horario> horarios = mItems.get(horario);

		if (horarios != null) {

			viewHolder.grid.removeAllViews();
			viewHolder.text.setText(String.format("%02d", horario));

			for (final Horario h : horarios) {

				ExtedendTextView v = (ExtedendTextView) LayoutInflater.from(mContext).inflate(R.layout.fragment_item_horario_linha, viewHolder.grid,
						false);

				String text = h.getHorarioSaida().toLocalTime().toString("mm");
				if (h.getDescricao() != null && !h.getDescricao().isEmpty()) {
					int index = mSublinhas.indexOf(h.getDescricao().trim().toUpperCase(Locale.getDefault()));
					text += String.format("<small><font color='#ff0000'><sup><b>(%d)</b></sup></font></small>", index + 1);
					v.setPadding(20, 0, 0, 0);
				}

				v.setText(Html.fromHtml(text));
				v.setLongClickable(true);

				if (proximoHorario.funcionaHoje() && proximoHorario.getHorarioSaida().toLocalTime().isAfter(LocalTime.now())
						&& proximoHorario.getCodigo() == h.getCodigo())
					v.setTypeface(null, Typeface.BOLD);

				v.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (mListener != null)
							mListener.horarioOnClick(h);
					}
				});
				v.setOnLongClickListener(new OnLongClickListener() {

					@Override
					public boolean onLongClick(View v) {
						if (mListener != null)
							mListener.horarioOnClick(h);

						return true;
					}
				});
				viewHolder.grid.addView(v);
			}
		}

		return convertView;
	}

	private class ViewHolder {
		TextView text;
		FlowLayout grid;
	}

	public interface AdapterDetalheHorarioListener {
		void horarioOnClick(Horario horario);

		void horarioOnPressed(Horario horario);
	}

}
