package br.srv.rcs.onibus.ui.adapter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apmem.tools.layouts.FlowLayout;
import org.apmem.tools.layouts.FlowLayout.LayoutParams;
import org.joda.time.LocalTime;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.srv.rcs.onibus.R;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.business.domain.Horario;

public class AdapterAlarme extends BaseAdapter {

	private Context context;
	private List<Alarme> items;
	private List<Integer> coresClaras;

	public AdapterAlarme(Context context, List<Alarme> data) {
		this.context = context;
		this.items = data;
		coresClaras = new ArrayList<Integer>();
		coresClaras.add(Color.parseColor("#ffffff"));
		coresClaras.add(Color.parseColor("#eeeeee"));
		coresClaras.add(Color.parseColor("#cccccc"));
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Alarme getItem(int position) {
		if (items.size() > position)
			return items.get(position);
		else
			return null;
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getCodigo();
	}

	public void insert(int position, Alarme item) {
		items.add(position, item);
	}

	public void remove(int position) {
		items.remove(position);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.fragment_item_alarme, parent, false);

			viewHolder = new ViewHolder();
			viewHolder.data = (LinearLayout) convertView.findViewById(R.id.linearHorario);
			viewHolder.data.setBackgroundResource(R.color.mark_red);
			viewHolder.horarioText = (TextView) convertView.findViewById(R.id.horarioText);
			viewHolder.dataText = (TextView) convertView.findViewById(R.id.dataText);
			viewHolder.linhaText = (TextView) convertView.findViewById(R.id.linhaText);
			viewHolder.grid = (FlowLayout) convertView.findViewById(R.id.grid);

			// Valor --Sempre virá
			viewHolder.valor = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.valor.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.valorText = (TextView) viewHolder.valor.findViewById(R.id.text);
			viewHolder.valorText.setBackgroundResource(R.color.mark_green);
			viewHolder.grid.addView(viewHolder.valor);

			// Proximo Horário
			viewHolder.proximoHorario = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.proximoHorario.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.proximoHorarioText = (TextView) viewHolder.proximoHorario.findViewById(R.id.text);
			viewHolder.proximoHorarioText.setBackgroundResource(R.color.mark_gray);
			viewHolder.grid.addView(viewHolder.proximoHorario);

			// Saída
			viewHolder.saidaEm = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.saidaEm.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.saidaEmText = (TextView) viewHolder.saidaEm.findViewById(R.id.text);
			viewHolder.saidaEmText.setBackgroundResource(R.color.mark_blue);
			viewHolder.grid.addView(viewHolder.saidaEm);

			// TODO: Adicionar novo alarme
			viewHolder.alarme = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.alarme.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.alarmeText = (TextView) viewHolder.alarme.findViewById(R.id.text);
			viewHolder.alarmeText.setBackgroundResource(R.color.mark_red);
			viewHolder.grid.addView(viewHolder.alarme);

			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		Alarme alarme = items.get(position);
		if (alarme != null) {

			Horario proximoHorario = alarme.getLinha().getProximoHorario();
			LocalTime horarioSaida = proximoHorario.getHorarioSaida().toLocalTime();
			// if (objectItem.getEmpresa() != null) {
			// viewHolder.image.setImageDrawable(objectItem.getImage());
			// viewHolder.image.setVisibility(View.VISIBLE);
			// }

			viewHolder.horarioText.setText(alarme.getHorario().toString("HH:mm"));
			viewHolder.data.setBackgroundColor(alarme.getCor());

			int color = Color.WHITE;
			if (coresClaras.contains(alarme.getCor()))
				color = Color.GRAY;

			viewHolder.dataText.setTextColor(color);
			viewHolder.horarioText.setTextColor(color);

			if (!alarme.isRepetirSemanalmente()) {
				viewHolder.dataText.setText(alarme.getData().toString("dd/MM/yyyy"));
				viewHolder.alarme.setVisibility(View.GONE);
			} else {

				viewHolder.dataText.setText(context.getString(R.string.repetir_semanalmente));

				viewHolder.alarme.setVisibility(View.VISIBLE);
				viewHolder.alarmeText.setText(alarme.getDiasDoAlarme());
			}

			viewHolder.linhaText.setText(alarme.getLinha().getNome());
			viewHolder.valorText.setText(NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(proximoHorario.getValor()));

			// Proximo Horário & Saída
			if (!horarioSaida.isAfter(LocalTime.now())) {
				viewHolder.proximoHorario.setVisibility(View.GONE);
				viewHolder.saidaEm.setVisibility(View.GONE);
			} else {
				// Próximo Horário
				String proximoHorarioTexto = String.format("<b>%s</b> %s", context.getString(R.string.proximo_horario_min),
						horarioSaida.toString("HH:mm'H'"));
				viewHolder.proximoHorarioText.setText(Html.fromHtml(proximoHorarioTexto));

				// Horário Saída
				int tempoEspera = proximoHorario.getTempoDeEspera();

				if (tempoEspera < 2)
					viewHolder.saidaEmText.setText(context.getString(R.string.format_saida_agora));
				else if (tempoEspera < 60)
					viewHolder.saidaEmText.setText(String.format(context.getString(R.string.format_saida_em), tempoEspera));
				else
					viewHolder.saidaEm.setVisibility(View.GONE);
			}

			// FIXME: Removendo por tamanho de tela
			viewHolder.saidaEm.setVisibility(View.GONE);
		}

		return convertView;
	}

	private class ViewHolder {

		LinearLayout data;
		TextView horarioText;
		TextView dataText;

		TextView linhaText;
		FlowLayout grid;

		FrameLayout valor;
		TextView valorText;

		FrameLayout saidaEm;
		TextView saidaEmText;

		FrameLayout proximoHorario;
		TextView proximoHorarioText;

		FrameLayout alarme;
		TextView alarmeText;
	}

}
