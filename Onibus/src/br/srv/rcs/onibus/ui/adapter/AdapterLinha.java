package br.srv.rcs.onibus.ui.adapter;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.apmem.tools.layouts.FlowLayout;
import org.apmem.tools.layouts.FlowLayout.LayoutParams;
import org.joda.time.LocalTime;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import br.srv.rcs.onibus.R;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Linha;

public class AdapterLinha extends BaseAdapter {

	private Context context;
	private List<Linha> items;
	private int layout;

	public AdapterLinha(Context context, List<Linha> data) {
		this.context = context;
		this.items = data;
		this.layout = R.layout.fragment_item_linha;
	}

	public AdapterLinha(Context context, List<Linha> data, int layout) {
		this.context = context;
		this.items = data;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Linha getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getCodigoExterno();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(layout, parent, false);

			viewHolder = new ViewHolder();
			//viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
			viewHolder.text = (TextView) convertView.findViewById(R.id.text);
			viewHolder.grid = (FlowLayout) convertView.findViewById(R.id.grid);

			// Valor --Sempre virá
			viewHolder.valor = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.valor.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.valorText = (TextView) viewHolder.valor.findViewById(R.id.text);
			viewHolder.valorText.setBackgroundResource(R.color.mark_green);
			viewHolder.grid.addView(viewHolder.valor);

			// Proximo Horário
			viewHolder.proximoHorario = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.proximoHorario.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.proximoHorarioText = (TextView) viewHolder.proximoHorario.findViewById(R.id.text);
			viewHolder.proximoHorarioText.setBackgroundResource(R.color.mark_gray);
			viewHolder.grid.addView(viewHolder.proximoHorario);

			// Saída
			viewHolder.saidaEm = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.saidaEm.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.saidaEmText = (TextView) viewHolder.saidaEm.findViewById(R.id.text);
			viewHolder.saidaEmText.setBackgroundResource(R.color.mark_blue);
			viewHolder.grid.addView(viewHolder.saidaEm);

			// TODO: Adicionar novo alarme
			viewHolder.alarme = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.fragment_item_linha_info, parent, false);
			viewHolder.alarme.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			viewHolder.alarmeText = (TextView) viewHolder.alarme.findViewById(R.id.text);
			viewHolder.alarmeText.setBackgroundResource(R.color.mark_red);
			viewHolder.alarmeText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_alarme_white, 0, 0, 0);
			viewHolder.grid.addView(viewHolder.alarme);

			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		Linha linha = items.get(position);
		if (linha != null) {

			Horario proximoHorario = linha.getProximoHorario();

			// Logo
			// if (linha.getEmpresa() != null) {
			// Drawable transparentDrawable = new
			// ColorDrawable(Color.TRANSPARENT);
			// Picasso.with(context).load(linha.getEmpresa().getLogo()).error(transparentDrawable).into(viewHolder.image);
			// }

			viewHolder.text.setText(linha.getNome());
			viewHolder.valorText.setText(NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(proximoHorario.getValor()));

			if (proximoHorario.funcionaHoje()) {
				LocalTime horarioSaida = proximoHorario.getHorarioSaida().toLocalTime();

				// Proximo Horário & Saída
				if (!horarioSaida.isAfter(LocalTime.now())) {
					viewHolder.proximoHorario.setVisibility(View.GONE);
					viewHolder.saidaEm.setVisibility(View.GONE);
				} else {
					viewHolder.proximoHorario.setVisibility(View.VISIBLE);
					viewHolder.saidaEm.setVisibility(View.VISIBLE);

					// Próximo Horário
					String proximoHorarioTexto = String.format("<b>%s</b> %s", context.getString(R.string.proximo_horario),
							horarioSaida.toString("HH:mm'H'"));
					viewHolder.proximoHorarioText.setText(Html.fromHtml(proximoHorarioTexto));

					// Horário Saída
					int tempoEspera = proximoHorario.getTempoDeEspera();
					if (tempoEspera < 2)
						viewHolder.saidaEmText.setText(context.getString(R.string.format_saida_agora));
					else if (tempoEspera < 60)
						viewHolder.saidaEmText.setText(String.format(context.getString(R.string.format_saida_em), tempoEspera));
					else
						viewHolder.saidaEm.setVisibility(View.GONE);
				}
			} else {
				viewHolder.proximoHorario.setVisibility(View.GONE);
				viewHolder.saidaEm.setVisibility(View.GONE);
			}

			// Alarme
			viewHolder.alarme.setVisibility(linha.isExisteAlarme() ? View.VISIBLE : View.GONE);
		}

		return convertView;
	}

	private class ViewHolder {
		TextView text;
		//ImageView image;
		FlowLayout grid;

		FrameLayout valor;
		TextView valorText;

		FrameLayout proximoHorario;
		TextView proximoHorarioText;

		FrameLayout saidaEm;
		TextView saidaEmText;

		FrameLayout alarme;
		TextView alarmeText;
	}

}
