package br.srv.rcs.onibus.ui.adapter;

import java.util.LinkedHashMap;
import java.util.Locale;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import br.srv.rcs.onibus.DetalheHorarioFragment;

public class PagerAdapterHorario extends FragmentStatePagerAdapter {

	private LinkedHashMap<String, Fragment> fragments;
	private int posicaoInicial = 0;

	public PagerAdapterHorario(FragmentManager fm) {
		super(fm);
		this.fragments = new LinkedHashMap<String, Fragment>();
	}

	public void addFragment(String title, Fragment fragment, boolean isSelected) {
		fragments.put(title, fragment);
		if (isSelected)
			posicaoInicial = fragments.size() - 1;
		
		notifyDataSetChanged();
	}

	@Override
	public Fragment getItem(int position) {
		return (Fragment) fragments.values().toArray()[position];
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return fragments.keySet().toArray()[position].toString().toUpperCase(Locale.getDefault());
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

	public int getPosicaoInicial() {
		return posicaoInicial;
	}

	public void moverParaHoraAtual(int position) {

		DetalheHorarioFragment fragment = (DetalheHorarioFragment) getItem(position);
		fragment.moverParaHoraAtual();
	}
}