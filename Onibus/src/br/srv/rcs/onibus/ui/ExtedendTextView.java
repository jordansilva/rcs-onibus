package br.srv.rcs.onibus.ui;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Spanned;
import android.util.AttributeSet;
import android.widget.TextView;
import br.srv.rcs.onibus.R;
import br.srv.rcs.onibus.R.styleable;

public class ExtedendTextView extends TextView {

	private static Map<String, Typeface> mTypefaces;
	private boolean mAllCaps;

	public ExtedendTextView(final Context context) {
		super(context);
	}

	public ExtedendTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		final TypedArray array = context.obtainStyledAttributes(attrs, styleable.ExtedendView);
		loadFontType(context, array);
		loadAllCaps(context, array);
	}

	public ExtedendTextView(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);

		final TypedArray array = context.obtainStyledAttributes(attrs, styleable.ExtedendView);

		loadFontType(context, array);
		loadAllCaps(context, array);
	}

	@SuppressLint("NewApi")
	private void loadAllCaps(final Context context, final TypedArray typedArray) {
		mAllCaps = typedArray.getBoolean(R.styleable.ExtedendView_allCaps, false);
		if (mAllCaps) {
			if (android.os.Build.VERSION.SDK_INT >= 14)
				setAllCaps(mAllCaps);
			else {
				if (getText() != null && getText().length() > 0) {
					String text = getText().toString().toUpperCase(Locale.getDefault());
					setText(text);
				}
			}
		}
	}

	private void loadFontType(final Context context, final TypedArray typedArray) {
		if (mTypefaces == null) {
			mTypefaces = new HashMap<String, Typeface>();
		}

		if (typedArray != null) {
			final String typefaceAssetPath = typedArray.getString(R.styleable.ExtedendView_font);

			if (typefaceAssetPath != null) {
				Typeface typeface = null;

				if (mTypefaces.containsKey(typefaceAssetPath)) {
					typeface = mTypefaces.get(typefaceAssetPath);
				} else {
					AssetManager assets = context.getAssets();
					typeface = Typeface.createFromAsset(assets, typefaceAssetPath);
					mTypefaces.put(typefaceAssetPath, typeface);
				}

				setTypeface(typeface);
			}
			typedArray.recycle();
		}
	}

	@Override
	public void setText(CharSequence text, BufferType type) {

		if (mAllCaps && text != null && text.length() > 0)
			text = text.toString().toUpperCase(Locale.getDefault());

		super.setText(text, type);
	}

	public void setTextHtml(Spanned text) {
		super.setText(text);
	}

	@SuppressLint("NewApi")
	public void setAllCaps(boolean allCaps) {

		if (android.os.Build.VERSION.SDK_INT >= 14)
			super.setAllCaps(allCaps);
		else
			mAllCaps = allCaps;
		
		invalidate();
	}

}
