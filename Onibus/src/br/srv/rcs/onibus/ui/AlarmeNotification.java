package br.srv.rcs.onibus.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import br.srv.rcs.onibus.DetalheLinhaActivity_;
import br.srv.rcs.onibus.R;
import br.srv.rcs.onibus.business.domain.Alarme;

public class AlarmeNotification {

	public static Notification criar(Context context, Alarme alarme) {

		Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

		Intent intent = new Intent(context, DetalheLinhaActivity_.class);
		intent.putExtra("linhaOnibus", alarme.getLinha());
		intent.putExtra("codigoLinha", alarme.getLinha().getCodigo());
		intent.setAction("actionstring" + System.currentTimeMillis());
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setSmallIcon(R.drawable.ic_launcher);
		builder.setContentTitle(getTitulo(alarme));
		builder.setContentText(getConteudo(alarme));
		builder.setDefaults(Notification.DEFAULT_VIBRATE);
		builder.setSound(sound);
		builder.setAutoCancel(true);
		builder.setContentIntent(pendingIntent);

		return builder.build();
	}

	private static String getTitulo(Alarme alarme) {
		return alarme.getLinha().getNome();
	}

	private static String getConteudo(Alarme alarme) {
		return "Alarme acionado: " + alarme.getHorario().toString("HH:mm");
	}
}
