package br.srv.rcs.onibus.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * @author Jordan Silva ( @jordansilva )
 * @since 05/01/2014
 */
public class NavMenuItem
{
	private int code;
	private String name;
	private Drawable image;

	public NavMenuItem(int code, String name)
	{
		this.code = code;
		this.name = name;
	}

	public NavMenuItem(int code, String name, Context context, int image)
	{
		this.code = code;
		this.name = name;
		this.image = context.getResources().getDrawable(image);
	}

	public NavMenuItem(int code, String name, Drawable drawable)
	{
		this.code = code;
		this.name = name;
		this.image = drawable;
	}

	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(int code)
	{
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the image
	 */
	public Drawable getImage()
	{
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(Drawable image)
	{
		this.image = image;
	}
	
	@Override
	public String toString()
	{
		return name;
	}

}
