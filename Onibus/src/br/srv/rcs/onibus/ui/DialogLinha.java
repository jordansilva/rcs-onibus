package br.srv.rcs.onibus.ui;

import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ArrayAdapter;

public class DialogLinha extends DialogFragment {

	private String mTitle;
	private List<Item> mList;
	private DialogLinhaListener mListener;

	public DialogLinha(String title, List<Item> list, DialogLinhaListener listener) {
		mTitle = title;
		mList = list;
		mListener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		ArrayAdapter<Item> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mList);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder = builder.setTitle(mTitle);
		builder = builder.setAdapter(adapter, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mListener != null)
					mListener.onItemClick(mList.get(which));
				
				dialog.dismiss();				
			}
		});
		return builder.create();
	}
	
	@Override
	public void onPause() {
		dismiss();
		super.onPause();
	}
	
	public interface DialogLinhaListener {
		void onItemClick(Item item);
	}
}