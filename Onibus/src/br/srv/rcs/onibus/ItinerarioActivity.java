package br.srv.rcs.onibus;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Itinerario;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.helper.GoogleMapsHelper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

@EActivity(R.layout.activity_itinerario)
public class ItinerarioActivity extends BaseActivity {

	private static final LatLng valeDoAcoLocation = new LatLng(-19.5283596, -42.620633);

	@Extra
	protected Linha linhaOnibus;

	@Extra
	protected Horario horarioOnibus;

	@ViewById
	protected TextView txtLinha;

	@ViewById
	protected TextView txtVia;

	@ViewById
	protected ImageView imgLinha;

	@ViewById
	protected TextView txtValor;

	@ViewById
	protected TextView txtHorario;

	protected GoogleMap map;

	@Bean
	OnibusService service;

	@Bean
	GoogleMapsHelper mapsHelper;

	@Override
	protected void load() {
		super.load();

		if (linhaOnibus != null && horarioOnibus != null) {
			buscarLinha();

		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_itinerario, menu);

		return super.onCreateOptionsMenu(menu);
	}

	protected void configurarUi() {

		configurarTexto();
		SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview));
		map = mapFragment.getMap();
		if (map != null) {
			configurarMap();
			mostrarRota();
		} else {
			findViewById(R.id.mapview).setVisibility(View.GONE);
		}
	}

	public OnClickListener getGoogleMapsListener() {
		return new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
				startActivity(intent);

				// Finish the activity so they can't circumvent the check
				finish();
			}
		};
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_alarme:
			AlarmeActivity_.intent(this).linhaOnibus(linhaOnibus).horarioOnibus(horarioOnibus).startForResult(1);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	protected void buscarLinha() {

		service.buscarLinha(linhaOnibus.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					linhaOnibus = (Linha) result.getData();
					configurarUi();

				}

			}
		});

	}

	protected void configurarTexto() {

		txtLinha.setText(linhaOnibus.getNome().trim());
		txtValor.setText(NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(horarioOnibus.getValor()));
		txtHorario.setText(horarioOnibus.getHorarioSaida().toString("HH:mm'H'"));

		if (horarioOnibus.getDescricao() != null && !horarioOnibus.getDescricao().isEmpty())
			txtVia.setText(horarioOnibus.getDescricao());
		else
			txtVia.setVisibility(View.GONE);

		if (linhaOnibus.getEmpresa() != null) {
			Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
			Picasso.with(this).load(linhaOnibus.getEmpresa().getLogo()).error(transparentDrawable).into(imgLinha);
		}
	}

	protected void configurarMap() {

		map.getUiSettings().setZoomControlsEnabled(false);

		CameraPosition cameraPosition = new CameraPosition.Builder().target(valeDoAcoLocation).zoom(11).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

	}

	protected void mostrarRota() {
		List<Itinerario> iHorario = (List<Itinerario>) linhaOnibus.getHorario(horarioOnibus.getCodigo()).getItinerarios();
		List<Itinerario> iLinha = (List<Itinerario>) linhaOnibus.getItinerarios();

		if (iHorario != null && iHorario.size() > 0)
			mostrarRota(iHorario);
		else if (iLinha != null && iLinha.size() > 0)
			mostrarRota(iLinha);
	}

	protected void mostrarRota(List<Itinerario> itinerarios) {

		Itinerario start = itinerarios.get(0);
		Itinerario end = itinerarios.get(itinerarios.size() - 1);

		MarkerOptions markerInicial = new MarkerOptions();
		markerInicial = markerInicial.position(new LatLng(start.getLatitude(), start.getLongitude()));
		markerInicial = markerInicial.title(start.getDescricao());

		MarkerOptions markerFinal = new MarkerOptions();
		markerFinal = markerFinal.position(new LatLng(end.getLatitude(), end.getLongitude()));
		markerFinal = markerFinal.title(end.getDescricao());

		map.addMarker(markerInicial);
		map.addMarker(markerFinal);

		PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.BLUE);

		for (Itinerario i : itinerarios) {
			rectLine.add(new LatLng(i.getLatitude(), i.getLongitude()));
		}

		map.addPolyline(rectLine);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		linhaOnibus = (Linha) savedInstanceState.get("linhaOnibus");
		horarioOnibus = (Horario) savedInstanceState.get("horarioOnibus");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("linhaOnibus", linhaOnibus);
		outState.putSerializable("horarioOnibus", horarioOnibus);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == RESULT_OK) {
			Toast.makeText(this, R.string.alarme_criado, Toast.LENGTH_SHORT).show();
			setResult(RESULT_OK);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}
}