package br.srv.rcs.onibus.service;

import org.springframework.http.converter.json.GsonHttpMessageConverter;

import br.srv.rcs.onibus.helper.GsonHelper;

import com.google.gson.Gson;

public class CustomHttpMessageConverter extends GsonHttpMessageConverter {

	protected static Gson buildGson() {
		return GsonHelper.getInstance();
	}

	public CustomHttpMessageConverter() {
		super(buildGson());
	}

}
