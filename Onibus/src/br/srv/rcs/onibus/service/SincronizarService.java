package br.srv.rcs.onibus.service;

import java.util.Collection;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.web.client.RestTemplate;

import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.business.domain.Propaganda;

@Rest(rootUrl = "http://sas4.hospedagemdesites.ws/Android/TransporteService.svc/", converters = { CustomHttpMessageConverter.class })
public interface SincronizarService extends RestClientErrorHandling {

	@Get("sincronizar/8bf3f976-6081-405c-9689-527e669e179b/{data}")
	Collection<Linha> sincronizar(String data);

	@Get("publicidade/8bf3f976-6081-405c-9689-527e669e179b")
	Collection<Propaganda> publicidade();
	
	RestTemplate getRestTemplate();
	void setRestTemplate(RestTemplate restTemplate);
}
