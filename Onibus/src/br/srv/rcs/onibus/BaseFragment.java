package br.srv.rcs.onibus;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.kanak.emptylayout.EmptyLayout;

@EFragment
public abstract class BaseFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@AfterViews
	protected void load() {
		configureActionBar();
		configureBackground();
	}

	protected void configureBackground() {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB)
			getView().setBackgroundColor(Color.parseColor("#fff3f3f3"));
	}

	protected void configureActionBar() {
		((BaseActivity) getActivity()).configureActionBar();
	}

	@UiThread
	protected void showEmpty(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showEmpty();
	}

	@UiThread
	protected void showLoading(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showLoading();
	}

	@UiThread
	protected void showError(EmptyLayout emptyLayout) {
		if (emptyLayout != null)
			emptyLayout.showError();
	}

	@UiThread
	protected void showToast(int texto) {
		if (getActivity() != null)
			Toast.makeText(getActivity(), texto, Toast.LENGTH_SHORT).show();
	}

}
