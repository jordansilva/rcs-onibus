package br.srv.rcs.onibus;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import br.srv.rcs.onibus.helper.Constants;

@EFragment(R.layout.fragment_fale_conosco)
public class FaleConoscoFragment extends BaseFragment {

	@ViewById
	protected Spinner spinnerAssunto;

	@ViewById
	protected EditText txtMensagem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();

		carregarUi();
	}

	@Override
	protected void configureActionBar() {
		// TODO Auto-generated method stub
		super.configureActionBar();
		getActivity().setTitle(R.string.title_fale_conosco);
	}

	protected void carregarUi() {

	}

	@Click(R.id.btnEnviar)
	protected void enviar(View v) {
		if (validarCampos())
			enviarEmail();
	}

	private boolean validarCampos() {
		boolean valid = true;

		String mensagem = txtMensagem.getText().toString();
		if (mensagem == null || mensagem.isEmpty()) {
			txtMensagem.setError(getString(R.string.field_required));
			txtMensagem.requestFocus();
			valid = false;
		}

		Object assunto = spinnerAssunto.getSelectedItem();
		if (assunto == null || spinnerAssunto.getSelectedItemPosition() == -1) {
			spinnerAssunto.requestFocus();
			valid = false;
		}

		return valid;
	}

	private void enviarEmail() {
		String[] destinatarios = new String[] { Constants.EMAIL_CONTATO };
		String assunto = (String) spinnerAssunto.getSelectedItem();
		String mensagem = txtMensagem.getText().toString();

		Intent email = new Intent(Intent.ACTION_SEND);
		email.setType("message/rfc822");
		email.putExtra(Intent.EXTRA_EMAIL, destinatarios);
		email.putExtra(Intent.EXTRA_SUBJECT, assunto);
		email.putExtra(Intent.EXTRA_TEXT, mensagem);
		email.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
		startActivity(Intent.createChooser(email, getString(R.string.intent_choose_email)));
	}
}
