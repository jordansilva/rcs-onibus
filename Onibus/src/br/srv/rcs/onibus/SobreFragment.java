package br.srv.rcs.onibus;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import br.srv.rcs.onibus.helper.Constants;

@EFragment(R.layout.fragment_sobre)
public class SobreFragment extends BaseFragment {

	@ViewById
	protected WebView webView;

	@ViewById
	protected TextView headerVersaoApp;

	@Override
	protected void load() {
		super.load();
		carregarTermosDeUso();
		carregarVersaoApp();
	}

	@Override
	protected void configureActionBar() {
		super.configureActionBar();
		getActivity().setTitle(R.string.menu_sobre);
	}

	protected void carregarVersaoApp() {
		try {
			String nomeVersao = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
			nomeVersao = headerVersaoApp.getText().toString().concat(" " + nomeVersao);
			headerVersaoApp.setText(nomeVersao);
		} catch (NameNotFoundException e) {
			headerVersaoApp.setVisibility(View.GONE);

		}
	}

	@Click(R.id.imageRCS)
	protected void clickImageRCS() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(Constants.URL_RCS));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	protected void carregarTermosDeUso() {
		webView.setBackgroundColor(0x00000000);
		webView.loadUrl("file:///android_asset/termos_de_uso.html");
	}

	// protected void carregarTermosDeUso() {
	// try {
	// AssetManager assets = getActivity().getAssets();
	// InputStream stream = assets.open("termos_de_uso.html");//
	// getResources().open(R.raw.termos_de_uso);
	// String termosDeUso = IOUtils.toString(stream);
	// IOUtils.closeQuietly(stream);
	// txtTermosDeUso.setText(Html.fromHtml(termosDeUso));
	//
	// } catch (IOException ex) {
	// ex.printStackTrace();
	// }
	// }

}
