package br.srv.rcs.onibus;

import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.srv.rcs.onibus.business.AlarmeService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Alarme;
import br.srv.rcs.onibus.ui.adapter.AdapterAlarme;

import com.kanak.emptylayout.EmptyLayout;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.OnDismissCallback;
import de.timroes.android.listview.EnhancedListView.Undoable;

@EFragment(R.layout.fragment_alarmes)
public class AlarmesFragment extends BaseFragment implements OnDismissCallback {

	private EmptyLayout mEmpty;
	private AdapterAlarme mAdapter;

	@ViewById
	protected EnhancedListView listView;

	@Bean
	protected AlarmeService service;

	@Override
	protected void load() {
		super.load();

		mEmpty = new EmptyLayout(getActivity(), listView);
		carregarMeusAlarmes();
	}

	@Override
	protected void configureActionBar() {
		// TODO Auto-generated method stub
		super.configureActionBar();
		getActivity().setTitle(R.string.menu_alarmes);
	}

	@Background
	protected void carregarMeusAlarmes() {

		service.listarMeusAlarmes(new ServiceListener() {

			@Override
			public void onPreInit() {
				showLoading(mEmpty);
			}

			@Override
			public void onPostExecute(final RequestResult result) {

				if (!result.isError()) {
					@SuppressWarnings("unchecked")
					List<Alarme> alarmes = (List<Alarme>) result.getData();
					carregarAlarmes(alarmes);

					if (alarmes.isEmpty())
						showEmpty(mEmpty);
				} else
					showError(mEmpty);
			}
		});
	}

	@UiThread
	protected void carregarAlarmes(List<Alarme> alarmes) {
		mAdapter = new AdapterAlarme(getActivity(), alarmes);
		listView.setAdapter(mAdapter);
		listView.setDismissCallback(this);
		listView.setRequireTouchBeforeDismiss(false);
		listView.enableSwipeToDismiss();
	}

	@Override
	public Undoable onDismiss(final EnhancedListView listView, final int position) {
		final Alarme item = mAdapter.getItem(position);
		if (item != null) {
			mAdapter.remove(position);

			return new Undoable() {

				@Override
				public void undo() {
					mAdapter.insert(position, item);
					mAdapter.notifyDataSetChanged();
				}

				@Override
				public void discard() {
					if (mAdapter.getCount() == 0) {
						listView.setAdapter(null);
						showEmpty(mEmpty);
					}
					removerAlarme(item);
				}

			};
		} else
			return new Undoable() {

				@Override
				public void undo() {
				}
			};
	}

	private void removerAlarme(Alarme alarme) {
		service.removerAlarme(alarme.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError())
					showToast(R.string.alarme_removido);
			}
		});
	}

}
