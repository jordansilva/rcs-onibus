package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import br.srv.rcs.onibus.business.OnibusService;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.domain.Linha;
import br.srv.rcs.onibus.ui.DialogLinha;
import br.srv.rcs.onibus.ui.DialogLinha.DialogLinhaListener;
import br.srv.rcs.onibus.ui.Item;
import br.srv.rcs.onibus.ui.LinearLayoutAnimation;
import br.srv.rcs.onibus.ui.RelativeLayoutAnimation;
import br.srv.rcs.onibus.ui.adapter.AdapterLinha;

import com.kanak.emptylayout.EmptyLayout;

@EFragment(R.layout.fragment_linhas)
public class LinhasFragment extends BaseFragment implements OnItemClickListener, OnItemLongClickListener {

	private EmptyLayout mEmptyMinhasLinhas;
	private MenuItem searchItem;

	@ViewById(R.id.lnlLinhas)
	protected LinearLayout lnlLinhas;

	@ViewById
	protected LinearLayoutAnimation lnlPesquisa;

	@ViewById
	protected RelativeLayoutAnimation lnlResultado;

	@ViewById
	protected AutoCompleteTextView txtDe;

	@ViewById
	protected AutoCompleteTextView txtPara;

	@ViewById
	protected TextView txtResultado;

	@ViewById
	protected ListView listView;

	@Bean
	protected OnibusService onibusService;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	protected void load() {
		super.load();

		mEmptyMinhasLinhas = new EmptyLayout(getActivity(), listView);
		adicionarListenerKeyboard();
		carregarItemsPesquisa();
	}

	@Override
	protected void configureActionBar() {
		// TODO Auto-generated method stub
		super.configureActionBar();
		getActivity().setTitle(R.string.title_pesquisar);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_linhas, menu);
		searchItem = menu.findItem(R.id.action_search);
		searchItem.setVisible(false);

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_todas_linhas:
			mostrarTodasAsLinhas();
			return true;
		case R.id.action_search:
			iniciaAnimacaoDaPesquisa();
			return true;
		}

		return true;
	}

	private void mostrarTodasAsLinhas() {
		Intent intent = new Intent(getActivity(), TodasLinhasActivity_.class);
		startActivity(intent);
	}

	@Background
	protected void carregarItemsPesquisa() {
		onibusService.listarDestinosParaPesquisa(new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					@SuppressWarnings("unchecked")
					List<String> linhas = (List<String>) result.getData();
					carregarAdapterPesquisa(linhas);
				}
			}
		});

	}

	@UiThread
	protected void carregarAdapterPesquisa(List<String> items) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, items);
		txtDe.setAdapter(adapter);
		txtDe.setTextColor(Color.BLACK);
		txtPara.setAdapter(adapter);
		txtPara.setTextColor(Color.BLACK);
	}

	@Click(R.id.btnPesquisar)
	@Background
	protected void pesquisarLinhas(View v) {

		String de = txtDe.getText().toString();
		String para = txtPara.getText().toString();

		if (!de.isEmpty() || !para.isEmpty()) {
			onibusService.buscarLinhas(de, para, new ServiceListener() {

				@Override
				public void onPreInit() {
					showLoading(mEmptyMinhasLinhas);
				}

				@Override
				public void onPostExecute(RequestResult result) {
					if (!result.isError()) {
						@SuppressWarnings("unchecked")
						List<Linha> linhas = (List<Linha>) result.getData();
						carregarLinhas(linhas);

						if (linhas.isEmpty())
							carregarLinhas(null);
						else
							esconderPesquisa();
					} else
						showError(mEmptyMinhasLinhas);
				}
			});
		} else
			carregarLinhas(null);
	}

	@UiThread
	protected void carregarLinhas(List<Linha> linhas) {
		txtResultado.setVisibility(View.VISIBLE);
		if (linhas != null) {

			listView.setAdapter(new AdapterLinha(getActivity(), linhas, R.layout.fragment_item_linha_list));
			listView.setOnItemClickListener(this);
			listView.setOnItemLongClickListener(this);
		} else {

			listView.setAdapter(null);
			showEmpty(mEmptyMinhasLinhas);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);
		DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

		final Linha linhaOnibus = (Linha) listView.getAdapter().getItem(position);

		List<Item> items = new ArrayList<Item>();
		items.add(new Item(1, getString(R.string.dialog_visualizar_detalhe)));
		items.add(new Item(2, getString(R.string.dialog_agendar_add)));
		if (linhaOnibus.isFavorito())
			items.add(new Item(3, getString(R.string.dialog_favoritos_del)));
		else
			items.add(new Item(3, getString(R.string.dialog_favoritos_add)));

		DialogLinha dialog = new DialogLinha(linhaOnibus.getNome(), items, new DialogLinhaListener() {

			@Override
			public void onItemClick(Item item) {
				if (item.getId() == 1)
					DetalheLinhaActivity_.intent(getActivity()).codigoLinha(linhaOnibus.getCodigo()).startForResult(1);

				if (item.getId() == 2)
					AlarmeActivity_.intent(getActivity()).linhaOnibus(linhaOnibus).startForResult(1);

				if (item.getId() == 3)
					favoritarLinha(linhaOnibus);

			}
		});
		dialog.show(getFragmentManager(), DialogLinha.class.getSimpleName());

		return true;
	}

	@UiThread
	protected void esconderPesquisa() {

		iniciaAnimacaoDaPesquisa();

		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(lnlPesquisa.getWindowToken(), 0);
	}

	protected void iniciaAnimacaoDaPesquisa() {
		final int height = (lnlPesquisa.getHeight() + lnlPesquisa.getPaddingTop());
		final RelativeLayoutAnimation rel = lnlResultado;

		if (lnlPesquisa.getVisibility() == View.INVISIBLE) {

			rel.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rel.getLayoutParams();
					params.topMargin += height;
					lnlPesquisa.setVisibility(View.VISIBLE);
					rel.setLayoutParams(params);
					searchItem.setVisible(false);
				}
			});

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, height);
			translate.setDuration(300);

			rel.startAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(0, 1);
			alpha.setDuration(300);
			lnlPesquisa.startAnimation(alpha);
		}

		if (lnlPesquisa.getVisibility() == View.VISIBLE) {

			rel.setAnimationListener(new RelativeLayoutAnimation.CustomAnimationListener() {

				@Override
				public void onAnimationEnd() {
					LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rel.getLayoutParams();
					params.topMargin -= height;
					lnlPesquisa.setVisibility(View.INVISIBLE);
					rel.setLayoutParams(params);
					searchItem.setVisible(true);
				}
			});

			TranslateAnimation translate = new TranslateAnimation(0, 0, 0, -height);
			translate.setDuration(300);

			rel.startAnimation(translate);

			AlphaAnimation alpha = new AlphaAnimation(1, 0);
			alpha.setDuration(300);
			lnlPesquisa.startAnimation(alpha);
		}

	}

	@Background
	protected void favoritarLinha(final Linha linha) {
		onibusService.favoritarLinha(linha.getCodigo(), new ServiceListener() {

			@Override
			public void onPreInit() {

			}

			@Override
			public void onPostExecute(RequestResult result) {
				if (!result.isError()) {
					int texto = linha.isFavorito() ? R.string.linha_favorito_removido : R.string.linha_favorito_adicionada;
					showToast(texto);
				}
			}
		});
	}

	private boolean propagandaVisible = true;

	private void adicionarListenerKeyboard() {

		final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, getResources().getDisplayMetrics());
		final DrawerActivity drawerActivity;
		if (getActivity() != null && isAdded()) {
			drawerActivity = (DrawerActivity) getActivity();
		} else
			drawerActivity = null;

		lnlLinhas.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				int heightDiff = lnlLinhas.getRootView().getHeight() - lnlLinhas.getHeight();
				if (propagandaVisible && drawerActivity != null)
					heightDiff = heightDiff - drawerActivity.getPropagandaHeight();

				propagandaVisible = !(heightDiff > padding);

				if (drawerActivity != null && isAdded())
					drawerActivity.setPropagandaVisible(propagandaVisible);
			}
		});
	}
}
