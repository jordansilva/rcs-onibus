package br.srv.rcs.onibus;

import java.util.ArrayList;
import java.util.Locale;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.joda.time.DateTime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.srv.rcs.onibus.business.RequestResult;
import br.srv.rcs.onibus.business.ServiceListener;
import br.srv.rcs.onibus.business.SincronismoService;
import br.srv.rcs.onibus.ui.MyPrefs_;
import br.srv.rcs.onibus.ui.NavMenuItem;
import br.srv.rcs.onibus.ui.adapter.AdapterMenu;

@EFragment(R.layout.fragment_menu)
public class MenuFragment extends Fragment implements OnItemClickListener {

	private AdapterMenu mAdapter;
	private MenuListener mListener;
	private DateTime ultimaSincronizacao;
	private static ProgressDialog progressDialog;
	private static ProgressDialogFragment newFragment;
	private static boolean isSincronizando;

	@ViewById(R.id.drawer_list)
	ListView mDrawerList;

	@ViewById
	TextView txtUltimaSincronizacao;

	@Bean
	SincronismoService service;

	@Pref
	MyPrefs_ myPrefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@AfterViews
	protected void load() {

		criarMenu();
		carregarUltimaSincronizacao();
	}

	protected void criarMenu() {
		ArrayList<NavMenuItem> data = new ArrayList<NavMenuItem>();

		data.add(new NavMenuItem(1, getString(R.string.menu_home).toUpperCase(Locale.getDefault()), getActivity(), R.drawable.ico_menu_home));
		data.add(new NavMenuItem(2, getString(R.string.menu_linhas).toUpperCase(Locale.getDefault()), getActivity(), R.drawable.ico_menu_onibus));
		data.add(new NavMenuItem(3, getString(R.string.menu_alarmes).toUpperCase(Locale.getDefault()), getActivity(), R.drawable.ico_menu_alarme));
		data.add(new NavMenuItem(4, getString(R.string.menu_favoritos).toUpperCase(Locale.getDefault()), getActivity(), R.drawable.ico_menu_favoritos));
		data.add(new NavMenuItem(5, getString(R.string.menu_fale_conosco).toUpperCase(Locale.getDefault()), getActivity(),
				R.drawable.ico_menu_faleconosco));
		data.add(new NavMenuItem(6, getString(R.string.menu_sobre).toUpperCase(Locale.getDefault()), getActivity(), R.drawable.ico_menu_sobre));

		mAdapter = new AdapterMenu(getActivity(), data);
		mDrawerList.setAdapter(mAdapter);
		mDrawerList.setOnItemClickListener(this);

	}

	@UiThread
	protected void carregarUltimaSincronizacao() {
		long sincronizacao = myPrefs.ultimaSincronizacao().get();

		if (sincronizacao == 0) {
			ultimaSincronizacao = null;
			txtUltimaSincronizacao.setText(R.string.nunca_sincronizou);
		} else {

			ultimaSincronizacao = new DateTime(sincronizacao);
			txtUltimaSincronizacao.setText(ultimaSincronizacao.toString("dd/MM/yyyy HH:mm"));
		}

	}

	@Click(R.id.relSincronizar)
	@Background(id = "sincronizar_task")
	protected void sincronizar() {

		if (isSincronizando)
			showDialog();
		else {

			DateTime dataSincronizacao = DateTime.now().minusYears(1);
			if (ultimaSincronizacao != null)
				dataSincronizacao = ultimaSincronizacao;

			service.sincronizar(dataSincronizacao, new ServiceListener() {

				@Override
				public void onPreInit() {
					isSincronizando = true;
					showDialog();
				}

				@Override
				public void onPostExecute(final RequestResult result) {

					isSincronizando = false;
					hideDialog();

					if (isAdded() && getActivity() != null) {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {

								if (!result.isError()) {
									myPrefs.edit().ultimaSincronizacao().put(DateTime.now().getMillis()).apply();
									carregarUltimaSincronizacao();

									Toast.makeText(getActivity(), R.string.sincronizando_sucesso, Toast.LENGTH_SHORT).show();
								} else
									showErroDialog();
							}
						});
					}
				}
			});
		}
	}

	public void setListener(MenuListener listener) {
		mListener = listener;
	}

	public interface MenuListener {
		public void onMenuSelected(int codigoMenu, String nomeMenu);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		NavMenuItem item = mAdapter.getItem(position);
		mListener.onMenuSelected(item.getCode(), item.getName());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mListener = (MenuListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@UiThread
	protected void showErroDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder = builder.setMessage(R.string.sincronizando_error);
		builder.setTitle(R.string.sincronizacao_title);
		builder = builder.setPositiveButton(R.string.tentar_novamente, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				sincronizar();
			}
		});

		builder = builder.setNegativeButton(R.string.cancelar, null);

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@UiThread
	public void showDialog() {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		if (newFragment == null)
			newFragment = new ProgressDialogFragment();

		Fragment fragment = fragmentManager.findFragmentByTag("Dialog");
		if (!newFragment.isAdded() && fragment == null)
			newFragment.show(fragmentManager, "Dialog");

		int currentOrientation = getResources().getConfiguration().orientation;
		if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	}

	@UiThread
	public void hideDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();

		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}

	public static class ProgressDialogFragment extends DialogFragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View v = super.onCreateView(inflater, container, savedInstanceState);
			setCancelable(false);
			return v;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			progressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.sincronizando), true, false);
			progressDialog.setCancelable(false);
			return progressDialog;
		}
	}

}
