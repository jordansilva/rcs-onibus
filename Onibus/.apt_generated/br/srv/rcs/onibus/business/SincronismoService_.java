//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.srv.rcs.onibus.business;

import android.content.Context;
import br.srv.rcs.onibus.service.CustomErrorHandler_;
import br.srv.rcs.onibus.service.SincronizarService_;

public final class SincronismoService_
    extends SincronismoService
{

    private Context context_;

    private SincronismoService_(Context context) {
        context_ = context;
        init_();
    }

    public static SincronismoService_ getInstance_(Context context) {
        return new SincronismoService_(context);
    }

    private void init_() {
        sincronizarService = new SincronizarService_();
        context = context_;
        customErrorHandler = CustomErrorHandler_.getInstance_(context_);
        init();
    }

    public void rebind(Context context) {
        context_ = context;
        init_();
    }

}
