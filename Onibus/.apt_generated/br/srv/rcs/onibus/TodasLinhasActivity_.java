//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.srv.rcs.onibus;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import br.srv.rcs.onibus.R.id;
import br.srv.rcs.onibus.R.layout;
import br.srv.rcs.onibus.business.OnibusService_;
import br.srv.rcs.onibus.business.domain.Linha;
import com.kanak.emptylayout.EmptyLayout;
import org.androidannotations.api.BackgroundExecutor;
import org.androidannotations.api.SdkVersionHelper;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class TodasLinhasActivity_
    extends TodasLinhasActivity
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    private Handler handler_ = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
        setContentView(layout.activity_todas_linhas);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        onibusService = OnibusService_.getInstance_(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static TodasLinhasActivity_.IntentBuilder_ intent(Context context) {
        return new TodasLinhasActivity_.IntentBuilder_(context);
    }

    public static TodasLinhasActivity_.IntentBuilder_ intent(Fragment supportFragment) {
        return new TodasLinhasActivity_.IntentBuilder_(supportFragment);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (((SdkVersionHelper.getSdkInt()< 5)&&(keyCode == KeyEvent.KEYCODE_BACK))&&(event.getRepeatCount() == 0)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        listView = ((ListView) hasViews.findViewById(id.listView));
        load();
    }

    @Override
    public void showEmpty(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TodasLinhasActivity_.super.showEmpty(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showLoading(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TodasLinhasActivity_.super.showLoading(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showError(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TodasLinhasActivity_.super.showError(emptyLayout);
            }

        }
        );
    }

    @Override
    public void carregarLinhas(final List<Linha> linhas) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TodasLinhasActivity_.super.carregarLinhas(linhas);
            }

        }
        );
    }

    @Override
    public void favoritarLinha(final Linha linha) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    TodasLinhasActivity_.super.favoritarLinha(linha);
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    @Override
    public void carregarMinhasLinhas() {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    TodasLinhasActivity_.super.carregarMinhasLinhas();
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    public static class IntentBuilder_ {

        private Context context_;
        private final Intent intent_;
        private Fragment fragmentSupport_;

        public IntentBuilder_(Context context) {
            context_ = context;
            intent_ = new Intent(context, TodasLinhasActivity_.class);
        }

        public IntentBuilder_(Fragment fragment) {
            fragmentSupport_ = fragment;
            context_ = fragment.getActivity();
            intent_ = new Intent(context_, TodasLinhasActivity_.class);
        }

        public Intent get() {
            return intent_;
        }

        public TodasLinhasActivity_.IntentBuilder_ flags(int flags) {
            intent_.setFlags(flags);
            return this;
        }

        public void start() {
            context_.startActivity(intent_);
        }

        public void startForResult(int requestCode) {
            if (fragmentSupport_!= null) {
                fragmentSupport_.startActivityForResult(intent_, requestCode);
            } else {
                if (context_ instanceof Activity) {
                    ((Activity) context_).startActivityForResult(intent_, requestCode);
                } else {
                    context_.startActivity(intent_);
                }
            }
        }

    }

}
