//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.srv.rcs.onibus;

import java.io.Serializable;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.srv.rcs.onibus.R.id;
import br.srv.rcs.onibus.R.layout;
import br.srv.rcs.onibus.business.AlarmeService_;
import br.srv.rcs.onibus.business.domain.Horario;
import br.srv.rcs.onibus.business.domain.Linha;
import com.kanak.emptylayout.EmptyLayout;
import org.androidannotations.api.BackgroundExecutor;
import org.androidannotations.api.SdkVersionHelper;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class AlarmeActivity_
    extends AlarmeActivity
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    public final static String HORARIO_ONIBUS_EXTRA = "horarioOnibus";
    public final static String LINHA_ONIBUS_EXTRA = "linhaOnibus";
    private Handler handler_ = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
        setContentView(layout.activity_alarme);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        injectExtras_();
        service = AlarmeService_.getInstance_(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static AlarmeActivity_.IntentBuilder_ intent(Context context) {
        return new AlarmeActivity_.IntentBuilder_(context);
    }

    public static AlarmeActivity_.IntentBuilder_ intent(Fragment supportFragment) {
        return new AlarmeActivity_.IntentBuilder_(supportFragment);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (((SdkVersionHelper.getSdkInt()< 5)&&(keyCode == KeyEvent.KEYCODE_BACK))&&(event.getRepeatCount() == 0)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        frameColor = ((FrameLayout) hasViews.findViewById(id.frameColor));
        lnlRepetirSemanalmente = ((LinearLayout) hasViews.findViewById(id.lnlRepetirSemanalmente));
        lnlDia = ((LinearLayout) hasViews.findViewById(id.lnlDia));
        txtHora = ((TextView) hasViews.findViewById(id.txtHora));
        txtDia = ((TextView) hasViews.findViewById(id.txtDia));
        {
            View view = hasViews.findViewById(id.lnlHora);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        AlarmeActivity_.this.clickHora();
                    }

                }
                );
            }
        }
        {
            View view = hasViews.findViewById(id.lnlDia);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        AlarmeActivity_.this.clickDia();
                    }

                }
                );
            }
        }
        {
            View view = hasViews.findViewById(id.frameColor);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        AlarmeActivity_.this.clickFrameColor(view);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkSegunda));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkTerca));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkQuarta));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkQuinta));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkSexta));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkSabado));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkDomingo));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkDias(buttonView, isChecked);
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.chkRepetirSemanalmente));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        AlarmeActivity_.this.checkRepetirSemanalmente(buttonView, isChecked);
                    }

                }
                );
            }
        }
        load();
    }

    private void injectExtras_() {
        Bundle extras_ = getIntent().getExtras();
        if (extras_!= null) {
            if (extras_.containsKey(HORARIO_ONIBUS_EXTRA)) {
                horarioOnibus = ((Horario) extras_.getSerializable(HORARIO_ONIBUS_EXTRA));
            }
            if (extras_.containsKey(LINHA_ONIBUS_EXTRA)) {
                linhaOnibus = ((Linha) extras_.getSerializable(LINHA_ONIBUS_EXTRA));
            }
        }
    }

    @Override
    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
        injectExtras_();
    }

    @Override
    public void showEmpty(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmeActivity_.super.showEmpty(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showError(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmeActivity_.super.showError(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showLoading(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmeActivity_.super.showLoading(emptyLayout);
            }

        }
        );
    }

    @Override
    public void salvarAlarme() {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    AlarmeActivity_.super.salvarAlarme();
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    public static class IntentBuilder_ {

        private Context context_;
        private final Intent intent_;
        private Fragment fragmentSupport_;

        public IntentBuilder_(Context context) {
            context_ = context;
            intent_ = new Intent(context, AlarmeActivity_.class);
        }

        public IntentBuilder_(Fragment fragment) {
            fragmentSupport_ = fragment;
            context_ = fragment.getActivity();
            intent_ = new Intent(context_, AlarmeActivity_.class);
        }

        public Intent get() {
            return intent_;
        }

        public AlarmeActivity_.IntentBuilder_ flags(int flags) {
            intent_.setFlags(flags);
            return this;
        }

        public void start() {
            context_.startActivity(intent_);
        }

        public void startForResult(int requestCode) {
            if (fragmentSupport_!= null) {
                fragmentSupport_.startActivityForResult(intent_, requestCode);
            } else {
                if (context_ instanceof Activity) {
                    ((Activity) context_).startActivityForResult(intent_, requestCode);
                } else {
                    context_.startActivity(intent_);
                }
            }
        }

        public AlarmeActivity_.IntentBuilder_ horarioOnibus(Horario horarioOnibus) {
            intent_.putExtra(HORARIO_ONIBUS_EXTRA, ((Serializable) horarioOnibus));
            return this;
        }

        public AlarmeActivity_.IntentBuilder_ linhaOnibus(Linha linhaOnibus) {
            intent_.putExtra(LINHA_ONIBUS_EXTRA, ((Serializable) linhaOnibus));
            return this;
        }

    }

}
