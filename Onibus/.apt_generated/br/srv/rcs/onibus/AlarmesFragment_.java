//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.srv.rcs.onibus;

import java.util.List;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import br.srv.rcs.onibus.R.layout;
import br.srv.rcs.onibus.business.AlarmeService_;
import br.srv.rcs.onibus.business.domain.Alarme;
import com.kanak.emptylayout.EmptyLayout;
import de.timroes.android.listview.EnhancedListView;
import org.androidannotations.api.BackgroundExecutor;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class AlarmesFragment_
    extends AlarmesFragment
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    private View contentView_;
    private Handler handler_ = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public View findViewById(int id) {
        if (contentView_ == null) {
            return null;
        }
        return contentView_.findViewById(id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView_ = super.onCreateView(inflater, container, savedInstanceState);
        if (contentView_ == null) {
            contentView_ = inflater.inflate(layout.fragment_alarmes, container, false);
        }
        return contentView_;
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        service = AlarmeService_.getInstance_(getActivity());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static AlarmesFragment_.FragmentBuilder_ builder() {
        return new AlarmesFragment_.FragmentBuilder_();
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        listView = ((EnhancedListView) hasViews.findViewById(br.srv.rcs.onibus.R.id.listView));
        load();
    }

    @Override
    public void showError(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmesFragment_.super.showError(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showLoading(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmesFragment_.super.showLoading(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showEmpty(final EmptyLayout emptyLayout) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmesFragment_.super.showEmpty(emptyLayout);
            }

        }
        );
    }

    @Override
    public void showToast(final int texto) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmesFragment_.super.showToast(texto);
            }

        }
        );
    }

    @Override
    public void carregarAlarmes(final List<Alarme> alarmes) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                AlarmesFragment_.super.carregarAlarmes(alarmes);
            }

        }
        );
    }

    @Override
    public void carregarMeusAlarmes() {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    AlarmesFragment_.super.carregarMeusAlarmes();
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    public static class FragmentBuilder_ {

        private Bundle args_;

        private FragmentBuilder_() {
            args_ = new Bundle();
        }

        public AlarmesFragment build() {
            AlarmesFragment_ fragment_ = new AlarmesFragment_();
            fragment_.setArguments(args_);
            return fragment_;
        }

    }

}
